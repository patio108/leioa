asgiref==3.2.10
certifi==2020.6.20
chardet==3.0.4
Django==2.2.16
django-admin-sortable==2.2.3
django-appconf==1.0.4
django-ckeditor==6.0.0
django-geojson==3.0.0
django-imagekit==4.0.2
django-js-asset==1.2.2
django-leaflet==0.27.1
djangorestframework==3.12.1
emoji==1.4.2
idna==2.10
jsonfield==3.0.0
pilkit==2.0
Pillow==7.2.0
python-magic==0.4.18
pytz==2020.1
requests==2.24.0
six==1.15.0
sqlparse==0.3.1
urllib3==1.25.10
