# python
import os 
import json
import logging
import requests
from emoji import UNICODE_EMOJI
from urllib.parse import urlsplit
from tempfile import TemporaryFile
# django
from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
from django.core.validators import validate_email
from django.core.files import File
# project
from django.conf import settings
from .models import Chat
from apps.map.models import Testimony

logger = logging.getLogger('django')

def has_emoji(str):
    for char in str:
        if char in UNICODE_EMOJI:
            return True
    return False

# Create your views here.
class TelegramWebhookView(View):

    def get(self, request, *args, **kwargs):
        logger.debug("GET request on webhook")
        return JsonResponse({"ko": "Rasklat!"})

    def post(self, request, *args, **kwargs):
        logger.debug("Starting a telegram dialog")
        try:
            logger.debug( request.body.decode('UTF-8')  )
            data     = json.loads( request.body.decode('UTF-8') )
            message  = data["message"]
            c        = message["chat"]
            chat_id  = c['id']
            user     = c['first_name']
            text     = message['text'] if 'text' in message else ''
            chat, created = Chat.objects.get_or_create(
                chat_id = chat_id,
                user    = user
            )
            media = 'photo' in message or 'video' in message or 'animation' in message
            if created:
                testimony = Testimony.objects.create()
                chat.current_testimony = testimony
                chat.save()
            else:
                testimony = chat.current_testimony
            if text != '/participar' and not testimony:
                self.send_message(
                    "¡No te entiendo muy bien! Si quieres subir un testimonio tienes que /participar",
                    chat_id
                )
                return JsonResponse({ "ok": "Succesful request" })
            if has_emoji( text ):
                self.send_message(
                    "Por favor, no uses emoji, porque no los entiendo!",
                    chat_id
                )
                return JsonResponse({ "ok": "Succesful request" })
            if text == '/participar':
                chat.current_testimony = Testimony.objects.create()
                chat.save()
                self.send_message(
                    "Gracias por tu participación! Nuestro bot te ayudará a subir un video corto con tu testimonio a la plataforma de patio108. "
                    "Por limitaciones de uso de telegram este video ha de ser corto, acerca de un minuto de duración. "
                    "Si quieres subir un video más largo puedes usar el formulario en nuestra web https://patio108.es",
                    chat_id
                )
                self.send_message(
                    "¿Cómo quieres que aparezca tu nombre el testimonio? (Puedes usar tu nombre, un apodo, etc.)",
                    chat_id
                )
            elif not testimony.name:
                logger.debug( "Asking name" )
                if media: 
                    self.send_message( "Mejor dame sólo un nombre!", chat_id )
                    return JsonResponse({ "ok": "Succesful request" })
                testimony.name = text
                testimony.save()
                self.send_message(
                    "Muy bien, %s! Ahora dame un correo electrónico que podamos usar para contactarte en caso necesario." % chat.user,
                    chat_id
                )
            elif not testimony.email:
                logger.debug( "Asking email" )
                if media: 
                    self.send_message( "Interesante, pero preferiría tan sólo un correo electrónico de contacto.", chat_id )
                    return JsonResponse({ "ok": "Succesful request" })
                try: 
                    valid = validate_email(text)
                    logger.debug( text )
                    testimony.email = text
                    testimony.save()
                    self.send_message(
                        "Gracias! ¿De qué va tu testimonio? Nuestras categorías son: cultura, espacio público, feminismos, infancia, "
                        "medioambiente, memoria, migraciones, movilidad, seguridad, turismo, vivienda. Si pones más de una la primera "
                        "será la principal.",
                        chat_id
                    )
                except:
                    self.send_message(
                        "El correo %s no parece válido, ¿estás seguro que lo has escrito bien? Introdúcelo de nuevo." % text,
                        chat_id
                    )
            elif not testimony.summary:
                logger.debug( "Asking category" )
                if media: 
                    self.send_message( "Mmm... bien, pero prefiriría saber sólamente de qué va tu testimonio.", chat_id )
                    return JsonResponse({ "ok": "Succesful request" })
                testimony.summary = text
                testimony.save()
                self.send_message(
                    "Gracias! ¿En qué lugar de Sevilla quieres que situemos tu testimonio?",
                    chat_id
                )

            elif not testimony.address:
                logger.debug( "Asking address" )
                if media: 
                    self.send_message( "Epa! Sólo te estamos pidiendo una dirección o lugar en Sevilla.", chat_id )
                    return JsonResponse({ "ok": "Succesful request" })
                testimony.address = text
                testimony.save()
                self.send_message(
                    "Genial. Sube el video con tu testimonio.",
                    chat_id
                )
            elif not testimony.video:
                logger.debug( "Video:" )
                if not 'video' in message and not 'animation' in message:
                    self.send_message( "Interesante, pero mejor sube el video de tu testimonio! :-)", chat_id)
                    return JsonResponse({ "ok": "Succesful request" })
                else:
                    fid = message['video']['file_id'] if 'video' in message else message['animation']['file_id']
                    url = 'https://api.telegram.org/bot%s/getFile?file_id=%s' % ( 
                        settings.TELEGRAM_BOT_TOKEN, 
                        fid
                    )
                    r = requests.get( url )
                    if r.status_code==200: 
                        try:
                            logger.debug( r.text )
                            file_url = json.loads(r.text)['result']['file_path']
                            url = 'https://api.telegram.org/file/bot%s/%s' % ( 
                                settings.TELEGRAM_BOT_TOKEN, 
                                file_url
                            )
                            # @see https://goodcode.io/articles/django-download-url-to-file-field/
                            with TemporaryFile() as tf:
                                r = requests.get( url, stream=True )
                                for chunk in r.iter_content(chunk_size=4096):
                                    tf.write(chunk)
                                testimony.video.save(
                                    os.path.basename( urlsplit(url).path ),
                                    File( tf )
                                )
                            testimony.save()
                            self.send_message( "Tu testimonio se ha subido con éxito. ¡Gracias por tu colaboración!", chat_id )
                            chat.current_testimony = None
                            chat.save()
                        except Exception as e:
                            logger.debug('Exceptions: ')
                            logger.debug( e )
                            self.send_message( "Hay problemas conectando con el servidor. Por favor inténtalo algo más tarde.", chat_id)
                    elif r.status_code==400:
                        self.send_message( "Parece que el video es demasiado grande. Si has subido videos con anterioridad con este bot, "
                                "prueba a borrarlos e inténtalo de nuevo. Si el video sigue dando problemas mejor usa el formulario en "
                                "https://patio108.es. ", 
                                chat_id)
                    else:
                        self.send_message( "Hay problemas conectando con el servidor. Por favor inténtalo algo más tarde.", chat_id)
                    
        except Exception as e:
            logger.debug('Exceptions: ')
            logger.debug( e )
            return JsonResponse({
                "ko" : "Bad Request"
            })
        return JsonResponse({
            "ok": "Succesful request"
        })

    @staticmethod
    def send_message(message, chat_id):
        data = {
            "chat_id"    : chat_id,
            "text"       : message,
            "parse_mode" : "Markdown",
        }
        try:
            response = requests.post(
                "https://api.telegram.org/bot%s/sendMessage" % settings.TELEGRAM_BOT_TOKEN, 
                data=data
            )
        except Exception as e:
            return JsonResponse({
                "ko" : "There was a problem connecting to Telegram API"
            })
