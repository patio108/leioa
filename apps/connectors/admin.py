# django
from django.contrib import admin
# project
from .models import Chat

admin.site.register(Chat, admin.ModelAdmin)