# django
from django.urls import path, re_path
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
# app
from . import views

app_name = 'webhooks'

urlpatterns = [
    path(
        'telegram',
        csrf_exempt( views.TelegramWebhookView.as_view() ),
        name='telegram'
    ),
]
