# django
from django.utils.translation import ugettext_lazy as _
from django.db import models

# project
from django.conf import settings
from apps.map.models import Testimony



class Chat(models.Model):

    chat_id = models.CharField(
        _('Id del chat'),
        max_length = 128,
        blank = True,
        null = True,
        help_text = _(
            'Identificador del chat.'
        )
    )
    user = models.CharField(
        _('User del chat'),
        max_length = 128,
        blank = True,
        null = True,
        help_text = _(
            'Nombre del usuario.'
        )
    )
    current_testimony = models.ForeignKey(
        Testimony,
        verbose_name = _('Testimonio asociado actualmente a este chat'),
        related_name='chat',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    def __str__(self):
        return self.user
