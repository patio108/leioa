# python
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# django
from django.core.management.base import (
    BaseCommand,
    CommandError
)
# project
from django.conf import settings

"""
A manage.py command to migrate a project given a path that exposes it in JSON
following the structure given in apps
"""

class Command(BaseCommand):

    """
    Adds the path to project resource
    """
    def add_arguments(self, parser):
        parser.add_argument(
            '--hashtag',
            type=str,
            help='Provide a comma-separated list of '
            'desired hashtags (without the #)',
        )
        parser.add_argument(
            '--limit',
            type=int,
            help='Provide a maximum number of posts '
            'to be imported',
        )

    """
    Imports posts from Instagram
    """
    def handle(self, *args, **options):
        hashtag = options['hashtag']
        limit   = options['limit']

        if hashtag:
            try:
                caps = DesiredCapabilities().FIREFOX
                caps["marionette"] = False
                options = webdriver.FirefoxOptions()
                options.set_headless()
                browser = webdriver.Firefox(capabilities=caps, firefox_options=options)
                browser.get('https://www.instagram.com/')
            except:
                print("There was a problem connecting to instagram in headless mode. Abort.")
                browser.close()
            # Login
            try:
                username_input = browser.find_element_by_css_selector("input[name='username']")
                password_input = browser.find_element_by_css_selector("input[name='password']")
                username_input.send_keys(settings.INSTAGRAM_USER)
                password_input.send_keys(settings.INSTAGRAM_PASSWORD)
                login_button = browser.find_element_by_xpath("//button[@type='submit']")
                login_button.click()
            except:
                print("Already logged-in")
            # If notifications modal click on dismiss
            try:
                notifications_button = browser.find_element_by_xpath("//button[contains(text(), 'Not Now')]")
                notifications_button.click()
            except:
                print("Notifications modal not dismissed")
            # Visit hashtag
            browser.get('https://www.instagram.com/explore/tags/%s/' % hashtag)
            # Get posts
            most_recent_anchor = "//h2[contains(text(), 'Most recent')]"
            most_recent_links_anchor = most_recent_anchor + "/following-sibling::div//a"
            links = browser.find_elements_by_xpath(most_recent_links_anchor)
            urls = [ l.get_attribute('href') for l in links ]
            print("List of posts: ", urls)
            if limit:
                urls = urls[:limit]
            """for url in urls:
                browser.get(url)
                image_anchor = '//article/header/following-sibling::div/following-sibling::div//img'
                image_src = browser.find_element_by_xpath(image_anchor).get_attribute('src')
                print(image_src)"""
            browser.close()
        else:
            raise CommandError('You have to provide a valid hashtag')
