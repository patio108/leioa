# python
import json, re, urllib
from selenium import webdriver
# django
from django.core.management.base import (
    BaseCommand,
    CommandError
)
# project
from django.conf import settings

"""
A manage.py command to migrate a project given a path that exposes it in JSON
following the structure given in apps
"""

class Command(BaseCommand):

    """
    Adds the path to project resource
    """
    def add_arguments(self, parser):
        parser.add_argument(
            '--hashtag',
            type=str,
            help='Provide a comma-separated list of '
            'desired hashtags (without the #)',
        )
        parser.add_argument(
            '--limit',
            type=int,
            help='Provide a maximum number of posts '
            'to be imported',
        )

    """
    Imports posts from Instagram
    """
    def handle(self, *args, **options):
        hashtag = options['hashtag']
        limit   = options['limit']
        if hashtag:
            # try:
            options = webdriver.FirefoxOptions()
            options.set_headless()
            browser = webdriver.Firefox(firefox_options=options)
            try:
                hashtag_url = 'https://www.instagram.com/explore/tags/%s/?__a=1' % hashtag
                print('Fetching %s' % hashtag_url)
                browser.get(hashtag_url)
            except:
                print("There was a problem connecting to Instagram. Abort.")
                browser.close()
            data = browser.find_element_by_xpath("//div[@id='json']").text
            posts_data = json.loads(data)['graphql']['hashtag']['edge_hashtag_to_media']['edges']
            if limit:
                posts_data = posts_data[:limit]
            for post_data in posts_data:
                item = post_data['node']
                post = {
                    'id'      : item['id'],
                    'slug'    : item['shortcode'],
                    'img'     : item['thumbnail_src'],
                    'caption' : item['edge_media_to_caption']['edges'][0]['node']['text'],
                    'date'    : item['taken_at_timestamp'],
                }
                if item['accessibility_caption']:
                    caption = re.search(
                        re.compile( r'(.+) in (.+). Image may' ),
                        item['accessibility_caption']
                    )
                    if caption:
                        place = caption.group(2)
                        place = re.sub(' with(.+)', '', place)
                        post['place'] = place
                        # get coords
                        safe_place = urllib.parse.quote(place, safe='')
                        geocoder_url = 'https://nominatim.openstreetmap.org/search?q=%s&format=json' % safe_place
                        print('Fetching %s' % geocoder_url )
                        try:
                            browser.get(geocoder_url)
                            osm_response  = browser.find_element_by_xpath("//div[@id='json']").text
                            response_data = json.loads(osm_response)
                            if len(response_data) > 0:
                                location = response_data[0]
                                post['lat'] = location['lat']
                                post['lon'] = location['lon']
                        except:
                            print("There was a problem connecting to Nominatim.")
                # print(post)
            browser.close()
        else:
            raise CommandError('You have to provide a valid hashtag')
