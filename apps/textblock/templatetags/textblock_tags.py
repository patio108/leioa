# python
import os
# django
from django import template
# project
from ..models import Textblock

register = template.Library()

@register.simple_tag
def textblock(slug):
    text = Textblock.objects.filter(
        slug=slug
    )
    if text:
        return text[0]
    return { 
        'title' : '%s_title' % slug,
        'content' : '%s_content' % slug
    }


@register.simple_tag
def get_navigation(which=None):
    if which:
        return Textblock.objects.filter(
            navigation_visibility=which
        ).order_by(
            'navigation_order'
        )
    return Textblock.objects.exclude(
        navigation_visibility='0'
    ).order_by(
        'navigation_order'
    )