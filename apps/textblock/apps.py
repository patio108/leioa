from django.apps import AppConfig

class TextblockConfig(AppConfig):
    name = 'textblock'
    verbose_name = "Bloques de texto"