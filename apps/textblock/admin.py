# django
from django.contrib import admin
# project
from .models import Textblock

# Register your models here.

class TextblockAdmin(admin.ModelAdmin):
    model        = Textblock
    list_display = ('title', 'title_es', 'navigation_visibility', 'navigation_order')
    ordering     = ('navigation_visibility', 'navigation_order',)
    fields       = ( 
        ('title', 'title_es',),
        ('content', 'content_es'),
        ('navigation_visibility', 'navigation_order'), 
    )

admin.site.register(Textblock, TextblockAdmin)
