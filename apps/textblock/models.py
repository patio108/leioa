# django
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from django.core.validators import MaxValueValidator, MinValueValidator
# contrib
from ckeditor_uploader.fields import RichTextUploadingField


class Textblock(models.Model):

    title = models.CharField(
        _('Título del bloque'),
        max_length=256,
        blank=False,
        null=True
    )
    slug = models.SlugField(
        _('Identificador'),
        blank=True
    )
    content = RichTextUploadingField(
        _('Contenido'),
        blank=True,
        null=True
    )
    navigation_visibility = models.CharField(
        _('Navegación'),
        default = 0,
        max_length=2,
        choices=(
            ('0', _('No')),
            ('1', _('Primaria')),
            ('2', _('Secundaria')),
        ),
        help_text = _(
            'Indica si el texto es accesible a través de la navegación del sitio. '    
            'La navegación primaria se sitúa sobre la columna de la izquierda. La '
            'secundaria en el mapa, abajo a la izquierda.'
        ) 
    )
    navigation_order = models.PositiveSmallIntegerField(
        _('Orden en la navegación'),
        default = 0,
        validators = [
            MinValueValidator(0), 
            MaxValueValidator(10)
        ],
        help_text = _(
            'Números menores implican mayor prioridad. '    
            'Esto significa que se situarán más a la izquierda en el menú.'
        )
    )
    title_es = models.CharField(
        _('Título del bloque en español'),
        max_length=256,
        blank=False,
        null=True
    )
    content_es = RichTextUploadingField(
        _('Contenido en español'),
        blank=True,
        null=True
    )

    def save(self, *args, **kwargs):
        """Populate automatically 'slug' field"""
        if not self.slug:
            self.slug = slugify(self.title)
        super(Textblock, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name        = _('bloque de texto')
        verbose_name_plural = _('bloques de texto')