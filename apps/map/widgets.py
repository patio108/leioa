# django
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string
# contrib
from leaflet.forms.widgets import LeafletWidget

class GeocodedLeafletWidget(forms.widgets.Textarea):
    """A custom leaflet widget, that geocodes its geolocation from other fields"""

    def __init__(self, attrs=None, submit_text='Localiza los datos introducidos', provider='google', sources=None, key=None):
        self.submit_text = submit_text
        self.provider    = provider
        self.key         = key
        super(GeocodedLeafletWidget, self).__init__(attrs)

    class Media:
        """Bind static assets to widget rendering"""
        css = {
            'all': (
                'leaflet/leaflet.css',
            )
        }
        js = ( 'leaflet/leaflet.js', 'map/js/geocode.js')

    def render(self, name, value, attrs=None, renderer=None):
        """Render widget"""
        attrs['class'] = 'geocode-widget__geometry'
        attrs['data-provider'] = self.provider
        attrs['data-key']      = self.key
        parent_widget = super(GeocodedLeafletWidget, self).render(name, value, attrs )
        geocode = render_to_string("geocoded-leaflet-widget.html", {
            'parent_widget' : parent_widget,
            'submit_text'   : self.submit_text,
        })
        return geocode
