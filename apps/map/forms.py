# django
from django import forms
from django.utils.translation import ugettext_lazy as _
# project
from .models import Testimony
from .widgets import GeocodedLeafletWidget

class TestimonyForm(forms.ModelForm):

    class Meta:
        model = Testimony
        fields = [
            'name',
            'category',
            'other_categories',
            'address',
            'video',
            'email',
            'legal'
        ]

    def __init__(self, *args, **kwargs):
        super(TestimonyForm,self).__init__(*args, **kwargs)
        self.fields['category'].empty_label = 'Elige una categoría'
        self.fields['email'].required = True
        self.fields['address'].required = True
