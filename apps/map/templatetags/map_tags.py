# python
import os.path
# django
from django import template
from django.utils.translation import ugettext_lazy as _
# project
from django.conf import settings
from .. import models

register = template.Library()

@register.simple_tag
def get_hoods():    
    hoods = models.Hood.objects.all()
    return hoods 
