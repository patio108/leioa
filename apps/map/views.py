# django
from django.shortcuts import render
from django.views.generic import TemplateView
# project
from .models import Testimony, TestimonyCategory
from .forms import TestimonyForm


class Map(TemplateView):

    template_name = 'pages/map.html'

    def get_context_data(self, *args, **kwargs):
        context = super(Map, self).get_context_data(*args, **kwargs)
        context['items'] = Testimony.objects.order_by('-creation_date')
        context['categories'] = TestimonyCategory.objects.all()
        context['form']       = TestimonyForm()
        return context
