# python
import random, json
# django
from django.core.management.base import (
    BaseCommand,
    CommandError
)
# project
from django.conf import settings
from ...models import Testimony, TestimonyCategory

"""
A manage.py command to migrate a project given a path that exposes it in JSON
following the structure given in apps
"""

def randomlatlon():
    pos = [
        -5.9700 + (random.random() - .5) * .25,
        37.3808 + (random.random() - .5) * .25
    ]
    return json.loads('{ "type": "Point", "coordinates": [%s, %s] }' % ( pos[0], pos[1] ))

class Command(BaseCommand):

    """
    Adds the path to project resource
    """
    def add_arguments(self, parser):
        parser.add_argument(
            '--limit',
            type=int,
            help='Provide a maximum number of posts to be created',
        )

    """
    Imports posts from Instagram
    """
    def handle(self, *args, **options):
        Testimony.objects.all().delete()
        limit      = options['limit'] if options['limit'] else 50
        categories = TestimonyCategory.objects.all()
        for i in range(limit):
            category = random.choice(categories)
            other_categories = random.sample( list(categories), 2)
            other_categories = [ i for i in other_categories if i != category ]
            t = Testimony.objects.create(
                name='[%s] [%s]' % (i, category.name),
                category=category,
                position=randomlatlon(),
            )
            t.other_categories.set( other_categories )
