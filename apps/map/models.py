# django
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.utils.text import slugify
from django.utils.timezone import now
from django.urls import reverse
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator 
from datetime import date
# contrib
from ckeditor_uploader.fields import RichTextUploadingField
from djgeojson.fields import PointField
from adminsortable.models import SortableMixin

# project
from django.conf import settings
from apps.model_utils.models import Category, Image, Link, Publishable
from . import validators

validator_video = validators.VideoTypeValidator()


class Hood(models.Model):
    
    name = models.CharField(
        _('Nombre del barrio'),
        max_length = 128,
        blank = False,
        null = True
    )
    
    name_es = models.CharField(
        _('Nombre del barrio en español'),
        max_length = 128,
        blank = True,
        null = True
    )
    
    lon = models.FloatField(
        _('Longitud del punto del barrio donde quieres centrar el mapa'),
        blank = True,
        null = True
    )
    
    lat = models.FloatField(
        _('Latitud del punto del barrio donde quieres centrar el mapa'),
        blank = True,
        null = True
    )
    
    class Meta:
        verbose_name        = _('barrio')
        verbose_name_plural = _('barrios')

    def __str__(self):
        return self.name


class TestimonyCategory(Category):

    content = RichTextUploadingField(
        _('Contenido'),
        blank=True,
        null=True
    )
    name_es = models.CharField(
        _('Nombre de la categoría en español'),
        max_length = 128,
        blank = False,
        null = True
    )
    content_es =  RichTextUploadingField(
        _('Contenido en español'),
        blank=True,
        null=True
    )
    image = models.ImageField(
        _('Imagen'),
        help_text = _(
            'Introduce opcionalmente una imagen de la categoría'
        ),
        upload_to='categories',
        null = True,
        blank = True,
    )

class Testimony(Publishable):

    name = models.CharField(
        _('Nombre'),
        max_length = 128,
        blank = False,
        null = True,
        help_text = _(
            'Nombre de la propuesta.'
        )
    )
    name_es = models.CharField(
        _('Nombre [es]'),
        max_length = 128,
        blank = False,
        null = True,
        help_text = _(
            'Nombre de la propuesta en español.'
        )
    )
    year = models.PositiveSmallIntegerField(
        _('Año de la propuesta'),
        blank = True,
        null = True,
        validators = [
            MinValueValidator(2000), 
            MaxValueValidator(2050)
        ],
        help_text = _(
            'Si lo dejas en blanco se usará el año actual.'
        )
    )
    person_name = models.CharField(
        _('Nombre de la persona que propone'),
        max_length = 128,
        blank = True,
        null = True,
    )
    person_age = models.PositiveSmallIntegerField(
        _('Edad de la persona que propone'),
        blank = True,
        null = True,
        validators = [
            MinValueValidator(1), 
            MaxValueValidator(100)
        ],
    )
    state = models.CharField(
        _('Estado de la propuesta'),
        max_length=2,
        choices=(
            ('0', _('No validada')),
            ('1', _('Validada')),
            ('2', _('Vehiculizada')),
        ),
        blank = False,
        default = 0,
        help_text = _(
            'Estado actual de la propuesta.'
        )
    )
    address = models.CharField(
        _('Lugar'),
        max_length = 128,
        blank = True,
        null = True,
        help_text = _(
            'Dirección de la propuesta. Esta información no se muestra, '
            'la puedes usar para guardar esta información de manera privada.'
        )
    )
    position = PointField(
        _('Sitúa tu historia'),
        blank = True,
        null = True,
        help_text = _(
            'Usa el localizador para encontrar tu ubicación pulsando en '
            'el botón "Buscar" tras escribir una dirección, por ejemplo: '
            '"Avenida de Miraflores" o "Plaza de España". '
            'Si todo es correcto esta se ubicará en el mapa. '
            'Si no la encuentras busca una cercana. Puedes ajustar la posición '
            'del marcador moviéndolo con el ratón pulsado.'
        )
    )
    category = models.ForeignKey(
        TestimonyCategory,
        verbose_name=_('¿De qué va principalmente tu historia?'),
        related_name='main_posts',
        blank=False,
        null=True,
        on_delete=models.SET_NULL,
    )
    other_categories = models.ManyToManyField(
        TestimonyCategory,
        verbose_name=_('¿Hay otras categorías relacionadas?'),
        related_name='other_posts',
        blank=True,
        help_text = _(
            'Manten presionado "Control" (o "Command" en un Mac) para seleccionar '
            'más de una opción o deseleccionar elementos.'
        )
    )
    summary = models.TextField(
        _('Descripción'),
        blank = False,
        null = True,
    )
    summary_es = models.TextField(
        _('Descripción [es]'),
        blank = False,
        null = True,
    )
    video = models.FileField(
        _('Sube el vídeo con tu historia'),
        help_text = _(
            'Puedes usar formatos ogg, mp4, avi, webm o mov'
        ),
        validators = [ validator_video ],
        blank=True,
        null=True
    )
    video_reply = models.FileField(
        _('Vídeo de respuesta'),
        help_text = _(
            'Puedes usar formatos ogg, mp4, avi, webm o mov'
        ),
        validators = [ validator_video ],
        blank=True,
        null=True
    )
    video_reply_text = models.TextField(
        _('Texto del video de respuesta'),
        blank = True,
        null = True,
        help_text = _(
            'Especifica opcionalmente un texto para acompañar '
            'al video de respuesta. Éste se mostará sobre el mismo.'
        )
    )
    video_reply_text_es = models.TextField(
        _('Texto del video de respuesta [es]'),
        blank = True,
        null = True,
        help_text = _(
            'Especifica opcionalmente un texto para acompañar '
            'al video de respuesta. Éste se mostará sobre el mismo.'
        )
    )
    name_reply = models.CharField(
        _('Nombre de la persona que responde'),
        max_length = 128,
        blank = True,
        null = True,
    )
    position_reply = models.CharField(
        _('Cargo de la persona que responde'),
        max_length = 128,
        blank = True,
        null = True,
    )
    position_reply_es = models.CharField(
        _('Cargo de la persona que responde [es]'),
        max_length = 128,
        blank = True,
        null = True,
    )
    link_url = models.URLField(
        _('Enlace a página externa'),
        blank = True,
        null = True,
        help_text = _(
            'Especifica opcionalmente un enlace a una web externa'
        ),
    )
    link_text = models.CharField(
        _('Texto del enlace'),
        max_length = 128,
        blank = True,
        null = True,
        help_text = _(
            'Especifica el texto del enlace. Si no pones nada se usará '
            'la misma URL del enlace.'
        )
    )
    link_helptext = models.CharField(
        _('Texto de ayuda del enlace'),
        max_length = 128,
        blank = True,
        null = True,
        help_text = _(
            'Especifica opcionalmente un texto para acompañar al enlace. '
            'Éste se mostrará sobre el mismo.'
        )
    )
    link_text_es = models.CharField(
        _('Texto del enlace [es]'),
        max_length = 128,
        blank = True,
        null = True,
        help_text = _(
            'Especifica el texto del enlace. Si no pones nada se usará '
            'la misma URL del enlace.'
        )
    )
    link_helptext_es = models.CharField(
        _('Texto de ayuda del enlace [es]'),
        max_length = 128,
        blank = True,
        null = True,
        help_text = _(
            'Especifica opcionalmente un texto para acompañar al enlace. '
            'Éste se mostrará sobre el mismo.'
        )
    )
    email = models.EmailField(
        _('Correo electrónico'),
        help_text = _(
            'Danos un correo electrónico para contactar contigo '
            'en caso necesario'
        ),
        blank=True,
        null=True
    )
    legal = models.BooleanField(
        _('Consentimiento'),
        blank=False,
        default=False,
        help_text = _(
            '<p>Consiento expresamente, conforme al Reglamento General de Protección '
            'de Datos 2016/679, a la cesión de mi imagen, proporcionada a el proyecto '
            'Patio108 a través de videotestimonio, siempre con el fin de desarrollar '
            'el proyecto Patio108.</p>'
            '<p>Los datos serán guardados por una duración indeterminada, si bien '
            'en cualquier momento el usuario podrá solicitar a Patio108 la supresión '
            'de los datos personales que le conciernan (a través de solicitud que '
            'se remitirá a la dirección de correo electrónico contacto@patio108.es).</p>'
        )
    )

    class Meta:
        verbose_name        = _('testimonio')
        verbose_name_plural = _('testimonios')

    def __str__(self):
        return self.name if self.name else 'Sin nombre todavía'

    def save(self, *args, **kwargs):
        """Populate automatically 'slug' field"""
        if not self.id and not self.slug:
            self.slug = slugify(self.name)
        if not self.year:
            self.year = date.today().year
        super(Testimony, self).save(*args, **kwargs)
