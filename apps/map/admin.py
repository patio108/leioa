# django
from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin
from django.contrib.admin.filters import SimpleListFilter
from django.utils.translation import ugettext_lazy as _
# contrib
from imagekit.processors import ResizeToFill
from imagekit.admin import AdminThumbnail
# project
from . import models
from apps.model_utils import admin as model_utils

# Actions

def publish(modeladmin, request, queryset):
    queryset.update(published=True)

publish.short_description = "Publica los elementos seleccionados"

def unpublish(modeladmin, request, queryset):
    queryset.update(published=False)

unpublish.short_description = "Despublica los elementos seleccionados"


# ModelAdmins

class NullFilterSpec(SimpleListFilter):
    title = u''

    parameter_name = u''

    def lookups(self, request, model_admin):
        return (
            ('1', 'Bai', ),
            ('0', 'Ez', ),
        )

    def queryset(self, request, queryset):
        kwargs = {
        '%s'%self.parameter_name : None,
        }
        if self.value() == '0':
            return queryset.filter(**kwargs)
        if self.value() == '1':
            return queryset.exclude(**kwargs)
        return queryset


class VideoFilter(NullFilterSpec):
    title = 'Tiene vídeo'
    parameter_name = 'video'

class VideoReplyFilter(NullFilterSpec):
    title = 'Tiene respuesta'
    parameter_name = 'video_reply'


class TestimonyAdmin(LeafletGeoAdmin):
    model        = models.Testimony
    ordering     = ('-year', 'creation_date',)
    list_filter  = (VideoFilter, VideoReplyFilter,) 
    list_display = ('name', 'name_es', 'year', 'address', 'summary', 'summary_es', 'update_date')
    fields       = ( 
        ('name', 'name_es', 'published'), 
        'year',
        ('person_name', 'person_age'),
        ('summary', 'summary_es'), 
        'email', 
        ('category', 'other_categories', 'state'), 
        (
            'link_url', 
            'link_text', 'link_helptext',
            'link_text_es', 'link_helptext_es'
        ), 
        'address', 
        'position', 
        ('video', 'video_reply', 'video_reply_text', 'video_reply_text_es'),
        ('name_reply', 'position_reply', 'position_reply_es'), 
    )


admin.site.register(models.Testimony, TestimonyAdmin)

class HoodAdmin(admin.ModelAdmin):
    list_display = ('name', 'name_es', 'lat', 'lon' )

admin.site.register(models.Hood, HoodAdmin)

class TestimonyCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'name_es' )
    fields       = ( 
        ('name', 'name_es',)
    )

admin.site.register(models.TestimonyCategory, TestimonyCategoryAdmin)
