# python
import os
import magic
import datetime
# django
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible

@deconstructible
class VideoTypeValidator(object):

    def __init__(self, mime_types=[
        'video/x-flv',
        'video/mp4',
        'application/x-mpegURL',
        'video/MP2T',
        'video/3gpp',
        'video/quicktime',
        'video/x-msvideo',
        'video/x-ms-wmv',
        'video/x-ms-af',
        'video/webm',
        'video/mpeg'
    ]):
        self.mime_types = mime_types

    def __call__(self, value):
        try:
            mime = magic.from_buffer(value.read(), mime=True)
            if mime not in self.mime_types:
                raise ValidationError(_(
                    "El archivo %s tiene formato %s. Revise los formatos permitidos" % (
                        value.name,
                        mime.split('/')[1]
                    )
                ))
        except ValueError:
            pass
