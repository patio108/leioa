# django
from django.urls import path, include
# contrib
from rest_framework import routers
# app
from . import views

urlpatterns = [
    path(
        'testimonies',
        views.TestimonyList.as_view(),
        name='get_all',
    ),
    # Create a new testimony
    path(
        'report',
        views.create_testimony,
        name='create_testimony'
    ),
    # Serialize all items in a CSV file
    path(
        'reports',
        views.get_testimonies,
        name='get_testimonies'
    )

]
