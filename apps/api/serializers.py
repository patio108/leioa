# django
from django.utils import translation
from django.utils.translation import ugettext_lazy as _
# contrib
from rest_framework import serializers
# project
from apps.map import models

def get_translation_in(language, s):
    with translation.override(language):
        return translation.gettext(s)

class TestimonyCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.TestimonyCategory
        fields = ( 'name', 'name_es' )


class TestimonySerializer(serializers.ModelSerializer):

    position         = serializers.JSONField()
    category         = serializers.SerializerMethodField()
    other_categories = serializers.SerializerMethodField()
    state            = serializers.SerializerMethodField()
    name             = serializers.SerializerMethodField()
    summary          = serializers.SerializerMethodField()
    video_reply_text = serializers.SerializerMethodField()
    position_reply   = serializers.SerializerMethodField()
    link_text        = serializers.SerializerMethodField()
    link_helptext    = serializers.SerializerMethodField()

    def get_category(self, obj):
        if obj.category:
            return obj.category.name if self.context.get('lang') == 'eu' else obj.category.name_es                     
        return ''

    def get_other_categories(self, obj):
        other_categories = []
        field = 'name' if self.context.get('lang') == 'eu' else 'name_es'
        for cat in obj.other_categories.all():
            other_categories.append( getattr(obj.category, field) )
        return other_categories          

    def get_state(self, obj):
        return {
            'id'   : obj.state, 
            'name' : get_translation_in(
                self.context.get('lang'), 
                obj.get_state_display()
            )
        }

    def get_name(self, obj):
        if self.context.get('lang') == 'eu':
            return obj.name if obj.name else obj.name_es            
        return obj.name_es if obj.name_es else obj.name if obj.name else ''

    def get_summary(self, obj):
        if self.context.get('lang') == 'eu':
            return obj.summary if obj.summary else obj.summary_es            
        return obj.summary_es if obj.summary_es else obj.summary

    def get_video_reply_text(self, obj):
        if self.context.get('lang') == 'eu':
            return obj.video_reply_text if obj.video_reply_text else obj.video_reply_text_es            
        return obj.video_reply_text_es if obj.video_reply_text_es else obj.video_reply_text

    def get_position_reply(self, obj):
        if self.context.get('lang') == 'eu':
            return obj.position_reply if obj.position_reply else obj.position_reply_es            
        return obj.position_reply_es if obj.position_reply_es else obj.position_reply

    def get_link_text(self, obj):
        if self.context.get('lang') == 'eu':
            return obj.link_text if obj.link_text else obj.link_text_es            
        return obj.link_text_es if obj.link_text_es else obj.link_text

    def get_link_helptext(self, obj):
        if self.context.get('lang') == 'eu':
            return obj.link_helptext if obj.link_helptext else obj.link_helptext_es            
        return obj.link_helptext_es if obj.link_helptext_es else obj.link_helptext


    class Meta:
        model = models.Testimony
        fields = (
            'pk',
            'creation_date',
            'name',
            'year',
            'state',
            'person_name', 
            'person_age',
            'summary',
            'position',
            'category',
            'other_categories',
            'video',
            'video_reply',
            'video_reply_text',
            'name_reply',
            'position_reply',
            'link_url',
            'link_text',
            'link_helptext',
        )
