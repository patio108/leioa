# django
import csv
from datetime import date
from django.utils.translation import ugettext_lazy as _
from django.utils import translation
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponse, JsonResponse
from django.contrib import messages
from django.core.mail import send_mail
# contrib
from rest_framework import viewsets
from rest_framework import generics
# project
from apps.map import models
from .serializers import TestimonySerializer
from apps.map.forms import TestimonyForm
from django.urls import reverse

def get_translation_in(language, s):
    with translation.override(language):
        return translation.gettext(s)
        
class TestimonyViewSet(viewsets.ModelViewSet):
    queryset = models.Testimony.objects.order_by('-creation_date')
    serializer_class = TestimonySerializer


class TestimonyList(generics.ListAPIView):
    
    serializer_class = TestimonySerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["lang"] = self.request.GET.get('lang')
        return context

    def get_queryset(self):
        return models.Testimony.objects.order_by('-creation_date')


# TODO: move into the viewset
@csrf_protect
def create_testimony(request):
    """ Creates a Clip object. """

    if request.method == 'POST' and request.is_ajax:
        data = request.POST
        form = TestimonyForm(
            data,
            files=request.FILES
        )
        if form.is_valid():
            new_testimony = form.save()
        else:
            return HttpResponse(
                form.errors.as_json(),
                content_type="application/json",
                status=400
            )
        send_mail(
            'Hay un nuevo testimonio en Patio108',
            'Puedes verlo en: %s  \n¡Hasta luego, m\'arma!' % request.build_absolute_uri(
                reverse('admin:map_testimony_change', args=(new_testimony.pk,))
            ),
            'no-reply@patio108.es',
            ['contacto@patio108.es'],
            fail_silently=False,
        )
        messages.success(request, "Gracias por tu testimonio, se publicará en cuanto lo revisemos!")
        return HttpResponse(status=200)
    return HttpResponse(status=403)


def get_testimonies(request):
    """ Get all testimonies in a CSV file """
    
    response = HttpResponse(content_type='text/csv')
    filename = 'ukdurango__' + date.today().isoformat() + '.csv'
    response['Content-Disposition'] = 'attachment; filename=' + filename

    writer = csv.writer(response)

    writer.writerow(["Mapa de activos de salud # " + date.today().strftime("%d/%m/%Y")])
    header = [
        _("Izena"),
        _("Deskribapena"),
        _("Posta"),
        _("Kategoria"),
        _("Beste kategoria"),
        _("Egoera"),
        _("Kokapena"),
        _("Bideoa"),
        _("Latitudea"),
        _("Luzera"),
        _("Sortze data"),
    ]
    writer.writerow(header)
    items = models.Testimony.objects.all()
    for item in items:
        row = [
            item.name,
            item.summary.strip(),
            item.email,
            item.category.name if item.category else None,
            "".join([ i.name for i in item.other_categories.all() ]),
            get_translation_in( 'eu', item.get_state_display() ),
            item.address,
            item.video.url if item.video else None,
            item.position['coordinates'][0] if item.position else None,
            item.position['coordinates'][1] if item.position else None,
            item.creation_date.strftime("%d/%m/%Y"),
        ]

        writer.writerow(row)

    return response
