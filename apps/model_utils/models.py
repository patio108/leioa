# django
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.utils.text import slugify
from django.utils.timezone import now
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.models import User

# contrib
from ckeditor_uploader.fields import RichTextUploadingField
from adminsortable.models import SortableMixin

# project
from . import validators, utils
from django.conf import settings

validate_image_size = validators.ImageSizeValidator({
    'min_width'  : 480,
    'min_height' : 480,
    'max_width'  : 1920,
    'max_height' : 1280 })
validate_image_type = validators.ImageTypeValidator(["jpeg", "png"])
validate_file_type  = validators.FileTypeValidator()
images_path         = utils.RenameImage("images/")
files_path          = utils.RenameFile("attachments/")


class Image(SortableMixin):
    """ Images """

    image_file = models.ImageField(
        _('Archivo de imagen'),
        blank=False,
        validators=[validate_image_size, validate_image_type],
        upload_to=images_path
    )
    caption = models.CharField(
        _('Texto alternativo/Pie de foto'),
        max_length=200,
        blank=False,
        null=True
    )
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE
    )
    object_id = models.PositiveIntegerField()
    source_content = GenericForeignKey(
        'content_type',
        'object_id'
    )
    order = models.PositiveIntegerField(
        default=0,
        editable=False,
        db_index=True
    )
    not_caption = models.BooleanField(
        _('No mostrar pie de foto'),
        default=True,
        help_text=_("No mostrar el pie de foto en las galerías")
    )
    views_featured = models.BooleanField(
        _('Destacada'),
        default=False,
        help_text=_("La imagen destacada será la que se muestre en las vistas")
    )

    class Meta:
        verbose_name = _('imagen')
        verbose_name_plural = _('imágenes')
        ordering = ['order']

    def __str__(self):
        return self.image_file.name


class Video(SortableMixin):
    """ Images """

    source_url = models.CharField(
        _('Video'),
        max_length=200,
        blank=True,
        null=True,
        help_text=_('Inserta la url de un video de Youtube o Vimeo')
    )
    caption = models.CharField(
        _('Pie de video opcional'),
        max_length=200,
        blank=True,
        null=True
    )
    not_caption = models.BooleanField(
        _('No mostrar pie de video'),
        default=True,
        help_text=_("No mostrar el pie de video en las galerías")
    )
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE
    )
    object_id = models.PositiveIntegerField()
    source_content = GenericForeignKey(
        'content_type',
        'object_id'
    )
    order = models.PositiveIntegerField(
        default=0,
        editable=False,
        db_index=True
    )

    class Meta:
        ordering = ['order']

    def __str__(self):
        """String representation of this model objects."""

        return self.source_content.name if self.source_content else "Video %s" % self.id


class Attachment(SortableMixin):
    """ Attachments """

    name = models.CharField(
        _('Nombre del archivo'),
        max_length=200,
        blank=False,
        null=True
    )
    attachment_file = models.FileField(
        _('Archivo adjunto'),
        blank=False,
        validators=[validate_file_type],
        upload_to=files_path
    )
    caption = models.CharField(
        _('Descripción opcional'),
        max_length=200,
        blank=True,
        null=True
    )
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE
    )
    object_id = models.PositiveIntegerField()
    source_content = GenericForeignKey(
        'content_type',
        'object_id'
    )
    order = models.PositiveIntegerField(
        default=0,
        editable=False,
        db_index=True
    )

    class Meta:
        verbose_name = _('adjunto')
        verbose_name_plural = _('adjuntos')
        ordering = ['order']

    def __str__(self):
        """String representation of this model objects."""

        return self.attachment_file.name


class Link(SortableMixin):
    """ Link """

    url = models.URLField(
        _('URL del enlace'),
        blank=False
    )
    caption = models.CharField(
        _('Texto del enlace'),
        max_length=200,
        blank=True
    )
    content_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE
    )
    object_id = models.PositiveIntegerField()
    source_content = GenericForeignKey(
        'content_type',
        'object_id'
    )
    order = models.PositiveIntegerField(
        default=0,
        editable=False,
        db_index=True
    )

    class Meta:
        verbose_name        = _('enlace')
        verbose_name_plural = _('enlaces')
        ordering            = ['order']

    def __str__(self):
        return self.url


class Category(models.Model):

    name = models.CharField(
        _('Nombre de la categoría'),
        max_length = 128,
        blank = False,
        null = True
    )
    slug = models.SlugField(
        blank = True,
        null = True
    )

    class Meta:
        abstract = True
        verbose_name        = _('categoría')
        verbose_name_plural = _('categorías')

    def __str__(self):
        """String representation of this model objects."""
        return self.name

    def save(self, *args, **kwargs):
        """Populate automatically 'slug' field"""
        if not self.slug:
            self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)


class Publishable(models.Model):
    """ An abstract class to represent publishable content with its
        meta fields. """

    slug = models.SlugField(
        _('Content slug'),
        blank=True
    )
    creation_date = models.DateField(
        _('Creation date'),
        editable=False,
        default=timezone.now
    )
    update_date = models.DateField(
        _('Last modified'),
        editable=False,
        auto_now=True,
    )
    published = models.BooleanField(
        _('Published'),
        blank=True,
        default=False,
    )
    owner = models.ForeignKey(
        User,
        verbose_name=_('Owner'),
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    class Meta:
        abstract = True
