# django
from django.contrib import admin
from django.db.models import ImageField
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string
from django.utils.html import mark_safe
# contrib
from adminsortable import admin as sortable
# project
from . import models

# django
from django.contrib.admin.widgets import AdminFileWidget

class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None, renderer=None):
        parent_widget = super(AdminImageWidget, self).render(name, value, attrs)
        c = {
            'parent_widget': parent_widget,
            'url'          : value,
        }
        return render_to_string("admin-image-widget.html", c)


# Register your models here.
class ImageInline(sortable.SortableGenericStackedInline):
    model  = models.Image
    extra  = 0
    fields = ('image_file', ('caption', 'not_caption', 'views_featured'),)

    formfield_overrides = {
        ImageField: {
            'widget': AdminImageWidget
        }
    }

class VideoInline(sortable.SortableGenericStackedInline):
    model  = models.Video
    extra  = 0
    fields = ('source_url', 'caption')

class AttachmentInline(sortable.SortableGenericStackedInline):
    model  = models.Attachment
    extra  = 0
    fields = ('attachment_file', 'name', 'caption',)

class LinkInline(sortable.SortableGenericStackedInline):
    model = models.Link
    extra = 0
    fields =  ('url', 'caption')
