# python
import os
# django
from django import template
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.utils.text import slugify
# project
from django.conf import settings

register = template.Library()

@register.simple_tag
def videoembed(src, w, h):
    uri           = src.split('://')[1]
    service       = uri.split('/')[0]
    common_attrs  = "frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen"
    resource      = ""
    if service == 'vimeo.com' or service == 'youtu.be':
        resource = "https://player.vimeo.com/video/%s" % uri.split('/')[1]
    elif service == 'www.youtube.com':
        if '?v=' in uri:
            resource = uri.split('?v=')[1]
        elif '&v=' in uri:
            resource = uri.split('&v=')[1]
        resource = "https://www.youtube.com/embed/%s" % resource
    elif service == 'youtu.be':
        resource = "https://www.youtube.com/embed/%s" % uri.split('/')[1]
    elif service == 'www.ccma.cat':
        resource = uri.split('/video/')[1]
        resource = "http://www.ccma.cat/video/embed/%s" % resource
    if resource is not "":
        return mark_safe("<iframe src='%s' width='%s' height='%s' %s></iframe>" % ( resource, w, h, common_attrs ))
    return ''

@register.filter
def filesize(url):
    return os.path.getsize(settings.BASE_DIR + url)

@register.filter
def file_exists(path):
    return os.path.isfile("%s/%s" % ( settings.BASE_DIR, path))

@register.filter
def media_exists(path):
    return os.path.isfile("%s/%s" % ( settings.MEDIA_ROOT, path))

@register.filter
def mediafile(path):
    imagefile = open("%s/%s" % ( settings.MEDIA_ROOT, path), "rb")
    return imagefile
