/**
 *  Common scripts
 */

document.addEventListener('DOMContentLoaded', function(){

    document.querySelectorAll('[data-show]').forEach( function(i){
        var to_hide = document.querySelectorAll(i.dataset.hide);
        var to_show = document.querySelector( i.dataset.show );
        i.addEventListener('click', function(e){
            e.stopPropagation();
            to_hide.forEach( function(i){
              i.classList.remove('visible');
            });
            to_show.classList.add('visible');
        })
    });
    document.querySelectorAll('[data-close]').forEach( function(i){
        var to_hide = document.querySelectorAll(i.dataset.close);
        i.addEventListener('click', function(){
            to_hide.forEach( function(i){
                i.classList.remove('visible');
            });
        })
    });
    document.querySelectorAll('[data-copy]').forEach( function(i){
        const value = i.dataset.copy;
        i.addEventListener('click', function(e){
            e.stopPropagation();
            i.classList.add('success');
            navigator.clipboard.writeText(value).then(i => {});
        });
    } );
    document.querySelectorAll('.site-message').forEach( function(i){
        i.addEventListener('click', function(e){
            i.remove();
        });
    });
    var form = document.querySelector('form');
    var form_container = document.querySelector('#contacta');
    if(form) form.addEventListener('submit', function(e){
        e.preventDefault();
        form_container.classList.add('loading');
        var data = new FormData(form);
        jQuery.ajax({
            type : form.method,
            url  : form.action,
            data : data,
            processData: false,
            contentType: false,
            success : function(response)
            {
                location.reload();
                form_container.classList.remove('loading');
            },
            error : function(response)
            {
                var errors_msg = JSON.parse(response.responseText);
                Object.keys(errors_msg).forEach( function(fieldname) {
                    var field = document.querySelector('.form-field--' + fieldname);
                    if(field){
                        field.classList.add('not-validated');
                        field.dataset.error = errors_msg[fieldname][0].message;
                    }
                });
                form_container.classList.remove('loading');
            },
        });
    });
});
