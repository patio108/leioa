/**
 * Webpack development configuration
 */

module.exports = {
    entry: {
        map : "./src/index.js",
    },
    mode: 'development',
    devtool: 'inline-source-map',
    output: {
        path: __dirname + "/dist",
        filename: "[name].js",
        publicPath: '/'
    },
    watch: true,
    optimization: {
        splitChunks: {
          chunks: 'all',
        },
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                }
            },
        ]
    },
    resolve : {
        extensions: [ '.js' ]
    }
}
