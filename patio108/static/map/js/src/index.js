import L from 'leaflet';
import { LoremIpsum } from "lorem-ipsum";
import slugify from "slugify";
var dayjs = require('dayjs')
var relativeTime = require('dayjs/plugin/relativeTime')
var dayjs_es = require('dayjs/locale/eu');
dayjs.extend(relativeTime);
dayjs.locale('eu');

// Detect if device is mobile
// Check breakpoints in sass so this value matches breakpoints in the layout
let is_mobile = window.matchMedia(
    "only screen and (max-width: 768px)"
).matches;

// Sets up lorem ipsum generator
const lorem = new LoremIpsum({
    sentencesPerParagraph : { max:  8, min: 4 },
    wordsPerSentence      : { max: 16, min: 4 }
});

let map;
let items_lookup  = [];
let item_selected = false;

let categories = {};
// Specify the name of the 'Other' category to put it in the last position in the menu
const category_other = ['Biak', 'Beste batzuk'];
const videos = document.querySelectorAll('video');
const lang   = window.location.pathname.indexOf('/es/') > -1 ? 'es' : 'eu';

// Leaflet map defaults
const MAP_DEFAULTS = {
    view : [ [ 43.3246031,-2.9769796 ], 14 ],
    providers : {
        osm : {
            tiles       : 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            attribution : '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        },
    },
    default_provider : 'osm',
}; 

/**
 *  Gets a random latlon near a given point
 *  @param {Array} point - Coords array in the form [ latitude, longitude ]
 *  @param {Float} distance_factor_lat - Distance multiplier -latitude
 *  @param {Float} distance_factor_lon - Distance multiplier - longitude
 */
const random_latlon = function(point, distance_factor_lat=.25, distance_factor_lon=.25 ){
    return [
        point[0] + (Math.random() - .5) * distance_factor_lat,
        point[1] + (Math.random() - .5) * distance_factor_lon
    ];
}

/**
 *  Gets a random integer
 *  @param {Integer} min - Minimum value
 *  @param {Integer} max - Maximum value
 */
const rint = function(min, max){
    return min + Math.floor( Math.random() * (max-min) );
}

/**
 *  Gets a random date
 *  @param {String} start - Start date in the format "yyyy-dd-mm"
 *  @param {String} end   - End date in the format "yyyy-dd-mm"
 */
function random_date(start, end) {
    var s = new Date( start ).getTime();
    var e = (end ? new Date(end) : new Date()).getTime();
    return rint(s,e);
}

/**
 *  Gets random categories
 *  @param {Integer} categories_number - Number of independent categories (not terms)
 */
function random_cats(categories_number) {
    let categories = [];
    let letters    = 'abcedefhij';
    for(let i = 0; i < categories_number; i++){
        const cat = letters.charAt(i);
        categories[ cat ] = [];
        let terms = rint(1, 6);
        for(let j = 0; j < terms; j++) {
            categories[cat].push(`t--${i}${j}`);
        }
    }
    return categories;
}


/**
 *  Hides all timeline items except the one selected
 *  @param {Integer} id - id of the selected item
 */
const select_item = function(id)
{
    // Timeline
    const posts = document.querySelectorAll('.timeline-item');
    posts.forEach( post => {
        const cls = post.classList;
        cls.add('hidden');
        if(cls.contains('selected')) cls.remove('selected');
    });
    const selected_post = document.querySelector(`.timeline-item.item-${id}`);
    selected_post.classList.remove('hidden');
    selected_post.classList.add('selected');
    // Map
    const markers = document.querySelectorAll('.leaflet-marker-icon');
    markers.forEach(marker => {
        marker.classList.add('inactive');
    });
    const selected_marker = document.querySelector(`.leaflet-marker-icon.item-${id}`);
    if(selected_marker){
        selected_marker.classList.remove('inactive');
        map.panTo( items_lookup.filter(i => i.pk == id)[0].latlon );
    }
    // All
    item_selected = true;
}

/**
 *  Shows all items in timeline if anyone is selected
 */
const show_all_timeline_items = function()
{
    var posts = document.querySelectorAll('.timeline-item');
    posts.forEach( post => {
        post.classList.remove('hidden');
        post.classList.remove('selected');
    });
    const markers = document.querySelectorAll('.leaflet-marker-icon');
    markers.forEach(marker => {
        marker.classList.remove('inactive');
    });
    item_selected = false;
    videos.forEach( function(video){
        video.pause();
    });
}

/**
 *  Returns the label of a given term_id
 *  @param {String} category_name - Name of the category
 *  @param {String} term_name - Name of the term
 */
const term_name = function(term_id){
    const terms = {};
    return term_id in terms ? terms[term_id] : term_id;
}

/**
 *  Sets up leaflet map
 *  @param {Map} map     - A variable to hold the leaflet map and make it accesible to other methods
 *  @param {Array} items - Array with data to set markers
 */
const init_map = function(items)
{
    // Create map
    map = L.map('map' , {
        scrollWheelZoom : !is_mobile,  // disable wheelzoom only in small devices
        zoomControl : false,
        minZoom: 12,
        maxZoom: 17
    }).setView(
        ...MAP_DEFAULTS.view
    );

    // Set up provider from defaults
    const provider = MAP_DEFAULTS.providers[
        MAP_DEFAULTS.default_provider
    ];

    // Set up tiles
    L.tileLayer( provider.tiles, {
        attribution: provider.attribution,
    }).addTo(map);

    // Load items
    if(items){
        items = items.filter( i => i.latlon.length > 0);
        const markers = items.map(i => {
            let classes = `item-${i.pk}`;
            const marker_img  = `/static/site/img/markers/marker-s${i.state}.svg`;
            let popup = `<h4 class="map-item__name">${ i.name }</h4><p class="map-item__summary">${ i.summary }</p>`;
            i.terms.forEach( term => { classes += ` ${ slugify(term).toLowerCase() }` });
            return L.marker( i.latlon, {
                icon : L.divIcon({
                    iconSize     : [ 40, 48 ],
                    shadowSize   : [ 40, 35 ],
                    shadowAnchor : [ 20, 0 ],
                    shadowUrl    : `/static/site/img/markers/marker-shadow.svg`,
                    html         : `<div class="inner"><img src="${marker_img}" /></div>`,
                    iconUrl      : marker_img,
                    className    : classes,
                })
            }).bindPopup( popup ).on('click', function(e){
                select_item(i.pk);
            })
        });
        L.layerGroup( markers ).addTo( map );
    }

    map.on('click', (e)=> {
        show_all_timeline_items();
    });
}

/**
 *  Update timeline
 *  @param {Array} items - Array with data to set items in the timeline
 */
const update_timeline = function(items)
{
    if(!items) return;
    items.forEach( i => {
      const item = document.querySelector(`.timeline-item.item-${ i.pk }`);
      item.querySelector('.timeline-item__date').innerHTML = dayjs(i.date).fromNow();
    });
}

const select_legend_category = function(item){
    const category = item.parentNode.dataset.cat;
    const category_terms = item.parentNode.querySelectorAll('.legend-category__item');
    if(item.dataset.action == 'remove_filters'){
        for(const c in categories){
            var cat = categories[c];
            for(const term in cat.terms)
                cat.terms[term] = true;
            cat.selected = document.querySelectorAll(`[data-cat='${c}'] .legend-category__item`).length;
        }
        document.querySelectorAll('.legend-category__item').forEach( i => {
            i.classList.add('on');
        });
        document.querySelectorAll('.leaflet-marker-icon').forEach( i => {
            i.classList.remove('hidden');
        });
        document.querySelectorAll('.timeline-item').forEach( i => {
            i.classList.remove('hidden');
        });
    };
    if(item.dataset.id)
    {
        const term_id = item.dataset.id;
        // If first term selected, uncheck all excepting itself
        if(categories[category].selected == category_terms.length){
            for(const term in categories[category].terms)
                categories[category].terms[term] = false;
            categories[category].terms[term_id] = true;
            category_terms.forEach( i => {
                i.classList.remove('on');
            })
            categories[category].selected = 1;
            item.classList.add('on');
        } else {
            categories[category].terms[term_id] = !categories[category].terms[term_id];
            item.classList.toggle('on');
            if(categories[category].terms[term_id])
                categories[category].selected++;
            else
                categories[category].selected--;
        }
        if(categories[category].selected == 0){
            for(const term in categories[category].terms)
                categories[category].terms[term] = true;
            category_terms.forEach( i => { i.classList.add('on'); })
            categories[category].selected = category_terms.length;
        }
        check_items();
    }
}

/**
 *  Sets up legend
 *  @param {Array} items - Array with data to set items in the timeline
 */
const init_legend = function(items)
{
    // Iterate over elements grabbing its categories
    items.forEach( item => {
        for(const cat in item.cats)
        {
            const terms = item.cats[cat];
            // If already created push only new terms into the category
            if(cat in categories){
                terms.forEach(term => {
                    if( !(term in categories[cat].terms) ){
                        categories[cat].terms[term] = true;
                    }
                });
            // Else push all item terms into the category
            } else {
                categories[cat] = {
                    'terms' : {},
                };
                terms.forEach(term => {
                    categories[cat].terms[term] = true;
                });
            }
            categories[cat].selected = Object.keys(categories[cat].terms).length;
        };
    });

    const categories_node = document.querySelector('.legend__categories');

    let remove_button = false;
    for(const cat in categories){
        // Create menu items
        let terms = '';
        const sorted = Object.keys( categories[cat].terms ).sort();

        // Put in last position 'Other' category
        if( category_other.length > 0 ){
            category_other.forEach( cat => {
                const ind = sorted.indexOf(cat);
                if(ind > -1) sorted.push( sorted.splice(ind, 1)[0] );
            });
        }
        if( sorted.length > 1){
            remove_button = true;
            sorted.forEach( function(term){ 
                if(term) {
                    terms += `\
                    <li class="legend-category__item on" data-id="${ slugify(term.toLowerCase() ) }">\
                        ${term}\
                    </li>`;
                }
            });
        }
        if(terms){
            var category_node = `\
            <div class="legend-category">\
                <ul class="legend-category__items" data-cat="${cat}">\
                    ${terms}\
                </ul>\
            </div>`;
            categories_node.innerHTML += category_node;
        }
    }
    if(remove_button) categories_node.innerHTML += `\
        <p class="legend-category__unfilter" data-action="remove_filters">\
            ${ lang == 'es' ? 'Quitar filtros' : 'Datuak ezabatu' }\
        </p>`
    ;

    categories_node.addEventListener('click', e => {
        e.stopPropagation();
        select_legend_category(e.target);
    });

    var legend = document.querySelector('.legend');
    document.querySelector('.legend__label').addEventListener('click', function(){
        legend.classList.toggle('collapsed');
    });
    if(is_mobile){
        legend.classList.add('collapsed')
    }
}

/**
 *  Checks markers and sets its visibility regarding selected categories
 */
const check_items = function()
{
    // If there's a selected timeline item, deselect it
    var selected = document.querySelector('.selected');
    if(selected) selected.classList.remove('selected');
    items_lookup.forEach( item =>
    {
        var item_displays = document.querySelectorAll(`.item-${item.pk}`);
        for(const category in categories){
            var is_visible = false;
            item_displays.forEach( d => { d.classList.remove('hidden') });
            Object.keys(categories[category].terms).forEach(id =>
            {
                if( categories[category].terms[id] && item.terms.includes( slugify(id).toLowerCase() ) ){
                    is_visible = true;
                    return;
                }
            });
            if(!is_visible){
                item_displays.forEach( d => {
                    d.classList.add('hidden')
                });
                return;
            }
        }
    });
}

/**
 *  Sets data items in proper format
 *  @param {Array} items - Array with data to set items
 */
const create_items = function(items)
{
    return items.map(i =>{
        let terms = [ i.category ];
        terms.push( ...i.other_categories );
        let all_terms = [...terms];
        all_terms.push( i.state.name );
        all_terms.push( i.year.toString() );
        return {
            pk      : i.pk,
            name    : i.name,
            state   : i.state.id,
            summary : i.summary,
            cats   : { 
                'category' : terms, 
                'state'    : [ i.state.name ],
                'year'     : [ i.year ],
            },
            main_term : slugify( i.category.toLowerCase() ),
            terms  : all_terms.map( j => slugify( j.toLowerCase() )),
            date   : i.creation_date,
            latlon : i.position ? i.position.coordinates.reverse() : [],
            video  : i.video,
        }
    });
}


/**
 *  Loads data from a remote endpoint
 *  @param {String} endpoint - URL of and endpoint that serves JSON data with the items array to be imported
 */
const load_data = function(endpoint)
{
    let items = [];

    if(endpoint){
        fetch(endpoint)
            .then( response => response.json() )
            .then( data => {
                items = create_items(data);
                items_lookup = items;
                init_map( items );
                init_legend( items );
                update_timeline( items );
                if(window.location.hash) {
                    const hash = window.location.hash.substring(1);
                    select_item(hash);
                }
                var last_edition= document.querySelector('.legend-category__items[data-cat="year"] li:last-child');
                select_legend_category( last_edition );
            });
    } else {
        const items_size = 20;
        items = new Array(items_size).fill().map( (i, n) => ({
            pk     : n,
            name   : lorem.generateWords( rint(3, 8) )
                          .replace( /^\w/, c => c.toUpperCase() ), // capitalize any sentence
            body   : lorem.generateSentences( rint(3, 5) ),
            img    : '/static/site/img/blank-image.svg',
            cats   : random_cats(2),
            date   : random_date("2020-01-01"),
            latlon : random_latlon( MAP_DEFAULTS.view[0] ),
            lnk    : '/',
        })).sort((a,b) => b.date - a.date);
        items.forEach(i => {
            i.date = new Date(i.date).toLocaleDateString('es-ES');
            i.terms = [];
            for(const cat in i.cats){
                i.cats[cat].forEach(term => {
                    i.terms.push(term);
                });
            };
        });
        items_lookup = items;
        init_map( items );
        init_timeline( items );
        init_legend( items );
    }
}

window.onload = ()=>{
    load_data(`/api/1.0/testimonies?lang=${lang}`);
    const not_reply_videos = document.querySelectorAll('video:not(.video-reply)');
    not_reply_videos.forEach( function(video){
        var pk = video.parentNode.parentNode.dataset.pk;
        video.addEventListener('play', function(e){
            select_item(pk);
            videos.forEach(function(i){
                if(i != e.target) {
                    i.pause();
                }
            });
        });
    });
    document.querySelectorAll('.timeline-item__back').forEach( function(i){
        i.addEventListener('click', function(e){
           e.stopPropagation();
           show_all_timeline_items();
        })
    });
    document.querySelector('.site-logo').addEventListener('click', function(e){
        show_all_timeline_items();
    });
    const hoodsearch = document.querySelector('.search-hood__widget');
    if(hoodsearch) hoodsearch.addEventListener('change', function(e){
        var latlon = hoodsearch.value.split(",");
        map.setView(latlon, 17);
    });
    document.querySelectorAll('.timeline-item').forEach( function(i){
        i.addEventListener('click', function(e){
           if(!i.classList.contains('selected')) {
              select_item(i.dataset.pk);
           }
        })
    });
};
