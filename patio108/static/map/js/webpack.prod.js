const TerserPlugin = require('terser-webpack-plugin');

/**
 * Webpack production configuration
 */

module.exports = {
    mode: 'production',
    entry: {
        map : "./src/index.js",
    },
    output: {
        path: __dirname + "/dist",
        filename: "[name].min.js",
        publicPath: '/'
    },
    optimization: {
        minimize: true,
        splitChunks: {
          chunks: 'all',
        },
        minimizer: [
            // Minimizer for app's code
            // to prevent app from breaking because of mangling
            new TerserPlugin({
                chunkFilter: (chunk) => {
                   if (chunk.name === 'vendors~map') return false;
                   return true;
                },
                terserOptions: {
                    mangle: false,
                },
            }),
            // Minimizer for vendors code
            new TerserPlugin({
                chunkFilter: (chunk) => {
                    if (chunk.name === 'map') return false;
                    return true;
                },
                terserOptions: {
                    mangle: true,
               },
            }),
        ],
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                }
            },
        ]
    },
    resolve : {
        extensions: [ '.js' ]
    }
};
