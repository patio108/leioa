/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"map": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/index.js","vendors~map"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lorem_ipsum__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! lorem-ipsum */ "./node_modules/lorem-ipsum/dist/index.js");
/* harmony import */ var lorem_ipsum__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lorem_ipsum__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var slugify__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! slugify */ "./node_modules/slugify/slugify.js");
/* harmony import */ var slugify__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(slugify__WEBPACK_IMPORTED_MODULE_2__);




var dayjs = __webpack_require__(/*! dayjs */ "./node_modules/dayjs/dayjs.min.js");

var relativeTime = __webpack_require__(/*! dayjs/plugin/relativeTime */ "./node_modules/dayjs/plugin/relativeTime.js");

var dayjs_es = __webpack_require__(/*! dayjs/locale/eu */ "./node_modules/dayjs/locale/eu.js");

dayjs.extend(relativeTime);
dayjs.locale('eu'); // Detect if device is mobile
// Check breakpoints in sass so this value matches breakpoints in the layout

let is_mobile = window.matchMedia("only screen and (max-width: 768px)").matches; // Sets up lorem ipsum generator

const lorem = new lorem_ipsum__WEBPACK_IMPORTED_MODULE_1__["LoremIpsum"]({
  sentencesPerParagraph: {
    max: 8,
    min: 4
  },
  wordsPerSentence: {
    max: 16,
    min: 4
  }
});
let map;
let items_lookup = [];
let item_selected = false;
let categories = {}; // Specify the name of the 'Other' category to put it in the last position in the menu

const category_other = ['Biak', 'Beste batzuk'];
const videos = document.querySelectorAll('video');
const lang = window.location.pathname.indexOf('/es/') > -1 ? 'es' : 'eu'; // Leaflet map defaults

const MAP_DEFAULTS = {
  view: [[43.3246031, -2.9769796], 14],
  providers: {
    osm: {
      tiles: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }
  },
  default_provider: 'osm'
};
/**
 *  Gets a random latlon near a given point
 *  @param {Array} point - Coords array in the form [ latitude, longitude ]
 *  @param {Float} distance_factor_lat - Distance multiplier -latitude
 *  @param {Float} distance_factor_lon - Distance multiplier - longitude
 */

const random_latlon = function (point, distance_factor_lat = .25, distance_factor_lon = .25) {
  return [point[0] + (Math.random() - .5) * distance_factor_lat, point[1] + (Math.random() - .5) * distance_factor_lon];
};
/**
 *  Gets a random integer
 *  @param {Integer} min - Minimum value
 *  @param {Integer} max - Maximum value
 */


const rint = function (min, max) {
  return min + Math.floor(Math.random() * (max - min));
};
/**
 *  Gets a random date
 *  @param {String} start - Start date in the format "yyyy-dd-mm"
 *  @param {String} end   - End date in the format "yyyy-dd-mm"
 */


function random_date(start, end) {
  var s = new Date(start).getTime();
  var e = (end ? new Date(end) : new Date()).getTime();
  return rint(s, e);
}
/**
 *  Gets random categories
 *  @param {Integer} categories_number - Number of independent categories (not terms)
 */


function random_cats(categories_number) {
  let categories = [];
  let letters = 'abcedefhij';

  for (let i = 0; i < categories_number; i++) {
    const cat = letters.charAt(i);
    categories[cat] = [];
    let terms = rint(1, 6);

    for (let j = 0; j < terms; j++) {
      categories[cat].push(`t--${i}${j}`);
    }
  }

  return categories;
}
/**
 *  Hides all timeline items except the one selected
 *  @param {Integer} id - id of the selected item
 */


const select_item = function (id) {
  // Timeline
  const posts = document.querySelectorAll('.timeline-item');
  posts.forEach(post => {
    const cls = post.classList;
    cls.add('hidden');
    if (cls.contains('selected')) cls.remove('selected');
  });
  const selected_post = document.querySelector(`.timeline-item.item-${id}`);
  selected_post.classList.remove('hidden');
  selected_post.classList.add('selected'); // Map

  const markers = document.querySelectorAll('.leaflet-marker-icon');
  markers.forEach(marker => {
    marker.classList.add('inactive');
  });
  const selected_marker = document.querySelector(`.leaflet-marker-icon.item-${id}`);

  if (selected_marker) {
    selected_marker.classList.remove('inactive');
    map.panTo(items_lookup.filter(i => i.pk == id)[0].latlon);
  } // All


  item_selected = true;
};
/**
 *  Shows all items in timeline if anyone is selected
 */


const show_all_timeline_items = function () {
  var posts = document.querySelectorAll('.timeline-item');
  posts.forEach(post => {
    post.classList.remove('hidden');
    post.classList.remove('selected');
  });
  const markers = document.querySelectorAll('.leaflet-marker-icon');
  markers.forEach(marker => {
    marker.classList.remove('inactive');
  });
  item_selected = false;
  videos.forEach(function (video) {
    video.pause();
  });
};
/**
 *  Returns the label of a given term_id
 *  @param {String} category_name - Name of the category
 *  @param {String} term_name - Name of the term
 */


const term_name = function (term_id) {
  const terms = {};
  return term_id in terms ? terms[term_id] : term_id;
};
/**
 *  Sets up leaflet map
 *  @param {Map} map     - A variable to hold the leaflet map and make it accesible to other methods
 *  @param {Array} items - Array with data to set markers
 */


const init_map = function (items) {
  // Create map
  map = leaflet__WEBPACK_IMPORTED_MODULE_0___default.a.map('map', {
    scrollWheelZoom: !is_mobile,
    // disable wheelzoom only in small devices
    zoomControl: false,
    minZoom: 12,
    maxZoom: 17
  }).setView(...MAP_DEFAULTS.view); // Set up provider from defaults

  const provider = MAP_DEFAULTS.providers[MAP_DEFAULTS.default_provider]; // Set up tiles

  leaflet__WEBPACK_IMPORTED_MODULE_0___default.a.tileLayer(provider.tiles, {
    attribution: provider.attribution
  }).addTo(map); // Load items

  if (items) {
    items = items.filter(i => i.latlon.length > 0);
    const markers = items.map(i => {
      let classes = `item-${i.pk}`;
      const marker_img = `/static/site/img/markers/marker-s${i.state}.svg`;
      let popup = `<h4 class="map-item__name">${i.name}</h4><p class="map-item__summary">${i.summary}</p>`;
      i.terms.forEach(term => {
        classes += ` ${slugify__WEBPACK_IMPORTED_MODULE_2___default()(term).toLowerCase()}`;
      });
      return leaflet__WEBPACK_IMPORTED_MODULE_0___default.a.marker(i.latlon, {
        icon: leaflet__WEBPACK_IMPORTED_MODULE_0___default.a.divIcon({
          iconSize: [40, 48],
          shadowSize: [40, 35],
          shadowAnchor: [20, 0],
          shadowUrl: `/static/site/img/markers/marker-shadow.svg`,
          html: `<div class="inner"><img src="${marker_img}" /></div>`,
          iconUrl: marker_img,
          className: classes
        })
      }).bindPopup(popup).on('click', function (e) {
        select_item(i.pk);
      });
    });
    leaflet__WEBPACK_IMPORTED_MODULE_0___default.a.layerGroup(markers).addTo(map);
  }

  map.on('click', e => {
    show_all_timeline_items();
  });
};
/**
 *  Update timeline
 *  @param {Array} items - Array with data to set items in the timeline
 */


const update_timeline = function (items) {
  if (!items) return;
  items.forEach(i => {
    const item = document.querySelector(`.timeline-item.item-${i.pk}`);
    item.querySelector('.timeline-item__date').innerHTML = dayjs(i.date).fromNow();
  });
};

const select_legend_category = function (item) {
  const category = item.parentNode.dataset.cat;
  const category_terms = item.parentNode.querySelectorAll('.legend-category__item');

  if (item.dataset.action == 'remove_filters') {
    for (const c in categories) {
      var cat = categories[c];

      for (const term in cat.terms) cat.terms[term] = true;

      cat.selected = document.querySelectorAll(`[data-cat='${c}'] .legend-category__item`).length;
    }

    document.querySelectorAll('.legend-category__item').forEach(i => {
      i.classList.add('on');
    });
    document.querySelectorAll('.leaflet-marker-icon').forEach(i => {
      i.classList.remove('hidden');
    });
    document.querySelectorAll('.timeline-item').forEach(i => {
      i.classList.remove('hidden');
    });
  }

  ;

  if (item.dataset.id) {
    const term_id = item.dataset.id; // If first term selected, uncheck all excepting itself

    if (categories[category].selected == category_terms.length) {
      for (const term in categories[category].terms) categories[category].terms[term] = false;

      categories[category].terms[term_id] = true;
      category_terms.forEach(i => {
        i.classList.remove('on');
      });
      categories[category].selected = 1;
      item.classList.add('on');
    } else {
      categories[category].terms[term_id] = !categories[category].terms[term_id];
      item.classList.toggle('on');
      if (categories[category].terms[term_id]) categories[category].selected++;else categories[category].selected--;
    }

    if (categories[category].selected == 0) {
      for (const term in categories[category].terms) categories[category].terms[term] = true;

      category_terms.forEach(i => {
        i.classList.add('on');
      });
      categories[category].selected = category_terms.length;
    }

    check_items();
  }
};
/**
 *  Sets up legend
 *  @param {Array} items - Array with data to set items in the timeline
 */


const init_legend = function (items) {
  // Iterate over elements grabbing its categories
  items.forEach(item => {
    for (const cat in item.cats) {
      const terms = item.cats[cat]; // If already created push only new terms into the category

      if (cat in categories) {
        terms.forEach(term => {
          if (!(term in categories[cat].terms)) {
            categories[cat].terms[term] = true;
          }
        }); // Else push all item terms into the category
      } else {
        categories[cat] = {
          'terms': {}
        };
        terms.forEach(term => {
          categories[cat].terms[term] = true;
        });
      }

      categories[cat].selected = Object.keys(categories[cat].terms).length;
    }

    ;
  });
  const categories_node = document.querySelector('.legend__categories');
  let remove_button = false;

  for (const cat in categories) {
    // Create menu items
    let terms = '';
    const sorted = Object.keys(categories[cat].terms).sort(); // Put in last position 'Other' category

    if (category_other.length > 0) {
      category_other.forEach(cat => {
        const ind = sorted.indexOf(cat);
        if (ind > -1) sorted.push(sorted.splice(ind, 1)[0]);
      });
    }

    if (sorted.length > 1) {
      remove_button = true;
      sorted.forEach(function (term) {
        if (term) {
          terms += `\
                    <li class="legend-category__item on" data-id="${slugify__WEBPACK_IMPORTED_MODULE_2___default()(term.toLowerCase())}">\
                        ${term}\
                    </li>`;
        }
      });
    }

    if (terms) {
      var category_node = `\
            <div class="legend-category">\
                <ul class="legend-category__items" data-cat="${cat}">\
                    ${terms}\
                </ul>\
            </div>`;
      categories_node.innerHTML += category_node;
    }
  }

  if (remove_button) categories_node.innerHTML += `\
        <p class="legend-category__unfilter" data-action="remove_filters">\
            ${lang == 'es' ? 'Quitar filtros' : 'Datuak ezabatu'}\
        </p>`;
  categories_node.addEventListener('click', e => {
    e.stopPropagation();
    select_legend_category(e.target);
  });
  var legend = document.querySelector('.legend');
  document.querySelector('.legend__label').addEventListener('click', function () {
    legend.classList.toggle('collapsed');
  });

  if (is_mobile) {
    legend.classList.add('collapsed');
  }
};
/**
 *  Checks markers and sets its visibility regarding selected categories
 */


const check_items = function () {
  // If there's a selected timeline item, deselect it
  var selected = document.querySelector('.selected');
  if (selected) selected.classList.remove('selected');
  items_lookup.forEach(item => {
    var item_displays = document.querySelectorAll(`.item-${item.pk}`);

    for (const category in categories) {
      var is_visible = false;
      item_displays.forEach(d => {
        d.classList.remove('hidden');
      });
      Object.keys(categories[category].terms).forEach(id => {
        if (categories[category].terms[id] && item.terms.includes(slugify__WEBPACK_IMPORTED_MODULE_2___default()(id).toLowerCase())) {
          is_visible = true;
          return;
        }
      });

      if (!is_visible) {
        item_displays.forEach(d => {
          d.classList.add('hidden');
        });
        return;
      }
    }
  });
};
/**
 *  Sets data items in proper format
 *  @param {Array} items - Array with data to set items
 */


const create_items = function (items) {
  return items.map(i => {
    let terms = [i.category];
    terms.push(...i.other_categories);
    let all_terms = [...terms];
    all_terms.push(i.state.name);
    all_terms.push(i.year.toString());
    return {
      pk: i.pk,
      name: i.name,
      state: i.state.id,
      summary: i.summary,
      cats: {
        'category': terms,
        'state': [i.state.name],
        'year': [i.year]
      },
      main_term: slugify__WEBPACK_IMPORTED_MODULE_2___default()(i.category.toLowerCase()),
      terms: all_terms.map(j => slugify__WEBPACK_IMPORTED_MODULE_2___default()(j.toLowerCase())),
      date: i.creation_date,
      latlon: i.position ? i.position.coordinates.reverse() : [],
      video: i.video
    };
  });
};
/**
 *  Loads data from a remote endpoint
 *  @param {String} endpoint - URL of and endpoint that serves JSON data with the items array to be imported
 */


const load_data = function (endpoint) {
  let items = [];

  if (endpoint) {
    fetch(endpoint).then(response => response.json()).then(data => {
      items = create_items(data);
      items_lookup = items;
      init_map(items);
      init_legend(items);
      update_timeline(items);

      if (window.location.hash) {
        const hash = window.location.hash.substring(1);
        select_item(hash);
      }

      var last_edition = document.querySelector('.legend-category__items[data-cat="year"] li:last-child');
      select_legend_category(last_edition);
    });
  } else {
    const items_size = 20;
    items = new Array(items_size).fill().map((i, n) => ({
      pk: n,
      name: lorem.generateWords(rint(3, 8)).replace(/^\w/, c => c.toUpperCase()),
      // capitalize any sentence
      body: lorem.generateSentences(rint(3, 5)),
      img: '/static/site/img/blank-image.svg',
      cats: random_cats(2),
      date: random_date("2020-01-01"),
      latlon: random_latlon(MAP_DEFAULTS.view[0]),
      lnk: '/'
    })).sort((a, b) => b.date - a.date);
    items.forEach(i => {
      i.date = new Date(i.date).toLocaleDateString('es-ES');
      i.terms = [];

      for (const cat in i.cats) {
        i.cats[cat].forEach(term => {
          i.terms.push(term);
        });
      }

      ;
    });
    items_lookup = items;
    init_map(items);
    init_timeline(items);
    init_legend(items);
  }
};

window.onload = () => {
  load_data(`/api/1.0/testimonies?lang=${lang}`);
  const not_reply_videos = document.querySelectorAll('video:not(.video-reply)');
  not_reply_videos.forEach(function (video) {
    var pk = video.parentNode.parentNode.dataset.pk;
    video.addEventListener('play', function (e) {
      select_item(pk);
      videos.forEach(function (i) {
        if (i != e.target) {
          i.pause();
        }
      });
    });
  });
  document.querySelectorAll('.timeline-item__back').forEach(function (i) {
    i.addEventListener('click', function (e) {
      e.stopPropagation();
      show_all_timeline_items();
    });
  });
  document.querySelector('.site-logo').addEventListener('click', function (e) {
    show_all_timeline_items();
  });
  const hoodsearch = document.querySelector('.search-hood__widget');
  if (hoodsearch) hoodsearch.addEventListener('change', function (e) {
    var latlon = hoodsearch.value.split(",");
    map.setView(latlon, 17);
  });
  document.querySelectorAll('.timeline-item').forEach(function (i) {
    i.addEventListener('click', function (e) {
      if (!i.classList.contains('selected')) {
        select_item(i.dataset.pk);
      }
    });
  });
};

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2luZGV4LmpzIl0sIm5hbWVzIjpbImRheWpzIiwicmVxdWlyZSIsInJlbGF0aXZlVGltZSIsImRheWpzX2VzIiwiZXh0ZW5kIiwibG9jYWxlIiwiaXNfbW9iaWxlIiwid2luZG93IiwibWF0Y2hNZWRpYSIsIm1hdGNoZXMiLCJsb3JlbSIsIkxvcmVtSXBzdW0iLCJzZW50ZW5jZXNQZXJQYXJhZ3JhcGgiLCJtYXgiLCJtaW4iLCJ3b3Jkc1BlclNlbnRlbmNlIiwibWFwIiwiaXRlbXNfbG9va3VwIiwiaXRlbV9zZWxlY3RlZCIsImNhdGVnb3JpZXMiLCJjYXRlZ29yeV9vdGhlciIsInZpZGVvcyIsImRvY3VtZW50IiwicXVlcnlTZWxlY3RvckFsbCIsImxhbmciLCJsb2NhdGlvbiIsInBhdGhuYW1lIiwiaW5kZXhPZiIsIk1BUF9ERUZBVUxUUyIsInZpZXciLCJwcm92aWRlcnMiLCJvc20iLCJ0aWxlcyIsImF0dHJpYnV0aW9uIiwiZGVmYXVsdF9wcm92aWRlciIsInJhbmRvbV9sYXRsb24iLCJwb2ludCIsImRpc3RhbmNlX2ZhY3Rvcl9sYXQiLCJkaXN0YW5jZV9mYWN0b3JfbG9uIiwiTWF0aCIsInJhbmRvbSIsInJpbnQiLCJmbG9vciIsInJhbmRvbV9kYXRlIiwic3RhcnQiLCJlbmQiLCJzIiwiRGF0ZSIsImdldFRpbWUiLCJlIiwicmFuZG9tX2NhdHMiLCJjYXRlZ29yaWVzX251bWJlciIsImxldHRlcnMiLCJpIiwiY2F0IiwiY2hhckF0IiwidGVybXMiLCJqIiwicHVzaCIsInNlbGVjdF9pdGVtIiwiaWQiLCJwb3N0cyIsImZvckVhY2giLCJwb3N0IiwiY2xzIiwiY2xhc3NMaXN0IiwiYWRkIiwiY29udGFpbnMiLCJyZW1vdmUiLCJzZWxlY3RlZF9wb3N0IiwicXVlcnlTZWxlY3RvciIsIm1hcmtlcnMiLCJtYXJrZXIiLCJzZWxlY3RlZF9tYXJrZXIiLCJwYW5UbyIsImZpbHRlciIsInBrIiwibGF0bG9uIiwic2hvd19hbGxfdGltZWxpbmVfaXRlbXMiLCJ2aWRlbyIsInBhdXNlIiwidGVybV9uYW1lIiwidGVybV9pZCIsImluaXRfbWFwIiwiaXRlbXMiLCJMIiwic2Nyb2xsV2hlZWxab29tIiwiem9vbUNvbnRyb2wiLCJtaW5ab29tIiwibWF4Wm9vbSIsInNldFZpZXciLCJwcm92aWRlciIsInRpbGVMYXllciIsImFkZFRvIiwibGVuZ3RoIiwiY2xhc3NlcyIsIm1hcmtlcl9pbWciLCJzdGF0ZSIsInBvcHVwIiwibmFtZSIsInN1bW1hcnkiLCJ0ZXJtIiwic2x1Z2lmeSIsInRvTG93ZXJDYXNlIiwiaWNvbiIsImRpdkljb24iLCJpY29uU2l6ZSIsInNoYWRvd1NpemUiLCJzaGFkb3dBbmNob3IiLCJzaGFkb3dVcmwiLCJodG1sIiwiaWNvblVybCIsImNsYXNzTmFtZSIsImJpbmRQb3B1cCIsIm9uIiwibGF5ZXJHcm91cCIsInVwZGF0ZV90aW1lbGluZSIsIml0ZW0iLCJpbm5lckhUTUwiLCJkYXRlIiwiZnJvbU5vdyIsInNlbGVjdF9sZWdlbmRfY2F0ZWdvcnkiLCJjYXRlZ29yeSIsInBhcmVudE5vZGUiLCJkYXRhc2V0IiwiY2F0ZWdvcnlfdGVybXMiLCJhY3Rpb24iLCJjIiwic2VsZWN0ZWQiLCJ0b2dnbGUiLCJjaGVja19pdGVtcyIsImluaXRfbGVnZW5kIiwiY2F0cyIsIk9iamVjdCIsImtleXMiLCJjYXRlZ29yaWVzX25vZGUiLCJyZW1vdmVfYnV0dG9uIiwic29ydGVkIiwic29ydCIsImluZCIsInNwbGljZSIsImNhdGVnb3J5X25vZGUiLCJhZGRFdmVudExpc3RlbmVyIiwic3RvcFByb3BhZ2F0aW9uIiwidGFyZ2V0IiwibGVnZW5kIiwiaXRlbV9kaXNwbGF5cyIsImlzX3Zpc2libGUiLCJkIiwiaW5jbHVkZXMiLCJjcmVhdGVfaXRlbXMiLCJvdGhlcl9jYXRlZ29yaWVzIiwiYWxsX3Rlcm1zIiwieWVhciIsInRvU3RyaW5nIiwibWFpbl90ZXJtIiwiY3JlYXRpb25fZGF0ZSIsInBvc2l0aW9uIiwiY29vcmRpbmF0ZXMiLCJyZXZlcnNlIiwibG9hZF9kYXRhIiwiZW5kcG9pbnQiLCJmZXRjaCIsInRoZW4iLCJyZXNwb25zZSIsImpzb24iLCJkYXRhIiwiaGFzaCIsInN1YnN0cmluZyIsImxhc3RfZWRpdGlvbiIsIml0ZW1zX3NpemUiLCJBcnJheSIsImZpbGwiLCJuIiwiZ2VuZXJhdGVXb3JkcyIsInJlcGxhY2UiLCJ0b1VwcGVyQ2FzZSIsImJvZHkiLCJnZW5lcmF0ZVNlbnRlbmNlcyIsImltZyIsImxuayIsImEiLCJiIiwidG9Mb2NhbGVEYXRlU3RyaW5nIiwiaW5pdF90aW1lbGluZSIsIm9ubG9hZCIsIm5vdF9yZXBseV92aWRlb3MiLCJob29kc2VhcmNoIiwidmFsdWUiLCJzcGxpdCJdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsUUFBUSxvQkFBb0I7UUFDNUI7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxpQkFBaUIsNEJBQTRCO1FBQzdDO1FBQ0E7UUFDQSxrQkFBa0IsMkJBQTJCO1FBQzdDO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsZ0JBQWdCLHVCQUF1QjtRQUN2Qzs7O1FBR0E7UUFDQTtRQUNBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN2SkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7O0FBQ0EsSUFBSUEsS0FBSyxHQUFHQyxtQkFBTyxDQUFDLGdEQUFELENBQW5COztBQUNBLElBQUlDLFlBQVksR0FBR0QsbUJBQU8sQ0FBQyw4RUFBRCxDQUExQjs7QUFDQSxJQUFJRSxRQUFRLEdBQUdGLG1CQUFPLENBQUMsMERBQUQsQ0FBdEI7O0FBQ0FELEtBQUssQ0FBQ0ksTUFBTixDQUFhRixZQUFiO0FBQ0FGLEtBQUssQ0FBQ0ssTUFBTixDQUFhLElBQWIsRSxDQUVBO0FBQ0E7O0FBQ0EsSUFBSUMsU0FBUyxHQUFHQyxNQUFNLENBQUNDLFVBQVAsQ0FDWixvQ0FEWSxFQUVkQyxPQUZGLEMsQ0FJQTs7QUFDQSxNQUFNQyxLQUFLLEdBQUcsSUFBSUMsc0RBQUosQ0FBZTtBQUN6QkMsdUJBQXFCLEVBQUc7QUFBRUMsT0FBRyxFQUFHLENBQVI7QUFBV0MsT0FBRyxFQUFFO0FBQWhCLEdBREM7QUFFekJDLGtCQUFnQixFQUFRO0FBQUVGLE9BQUcsRUFBRSxFQUFQO0FBQVdDLE9BQUcsRUFBRTtBQUFoQjtBQUZDLENBQWYsQ0FBZDtBQUtBLElBQUlFLEdBQUo7QUFDQSxJQUFJQyxZQUFZLEdBQUksRUFBcEI7QUFDQSxJQUFJQyxhQUFhLEdBQUcsS0FBcEI7QUFFQSxJQUFJQyxVQUFVLEdBQUcsRUFBakIsQyxDQUNBOztBQUNBLE1BQU1DLGNBQWMsR0FBRyxDQUFDLE1BQUQsRUFBUyxjQUFULENBQXZCO0FBQ0EsTUFBTUMsTUFBTSxHQUFHQyxRQUFRLENBQUNDLGdCQUFULENBQTBCLE9BQTFCLENBQWY7QUFDQSxNQUFNQyxJQUFJLEdBQUtqQixNQUFNLENBQUNrQixRQUFQLENBQWdCQyxRQUFoQixDQUF5QkMsT0FBekIsQ0FBaUMsTUFBakMsSUFBMkMsQ0FBQyxDQUE1QyxHQUFnRCxJQUFoRCxHQUF1RCxJQUF0RSxDLENBRUE7O0FBQ0EsTUFBTUMsWUFBWSxHQUFHO0FBQ2pCQyxNQUFJLEVBQUcsQ0FBRSxDQUFFLFVBQUYsRUFBYSxDQUFDLFNBQWQsQ0FBRixFQUE2QixFQUE3QixDQURVO0FBRWpCQyxXQUFTLEVBQUc7QUFDUkMsT0FBRyxFQUFHO0FBQ0ZDLFdBQUssRUFBUyxvREFEWjtBQUVGQyxpQkFBVyxFQUFHO0FBRlo7QUFERSxHQUZLO0FBUWpCQyxrQkFBZ0IsRUFBRztBQVJGLENBQXJCO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLE1BQU1DLGFBQWEsR0FBRyxVQUFTQyxLQUFULEVBQWdCQyxtQkFBbUIsR0FBQyxHQUFwQyxFQUF5Q0MsbUJBQW1CLEdBQUMsR0FBN0QsRUFBa0U7QUFDcEYsU0FBTyxDQUNIRixLQUFLLENBQUMsQ0FBRCxDQUFMLEdBQVcsQ0FBQ0csSUFBSSxDQUFDQyxNQUFMLEtBQWdCLEVBQWpCLElBQXVCSCxtQkFEL0IsRUFFSEQsS0FBSyxDQUFDLENBQUQsQ0FBTCxHQUFXLENBQUNHLElBQUksQ0FBQ0MsTUFBTCxLQUFnQixFQUFqQixJQUF1QkYsbUJBRi9CLENBQVA7QUFJSCxDQUxEO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsTUFBTUcsSUFBSSxHQUFHLFVBQVMzQixHQUFULEVBQWNELEdBQWQsRUFBa0I7QUFDM0IsU0FBT0MsR0FBRyxHQUFHeUIsSUFBSSxDQUFDRyxLQUFMLENBQVlILElBQUksQ0FBQ0MsTUFBTCxNQUFpQjNCLEdBQUcsR0FBQ0MsR0FBckIsQ0FBWixDQUFiO0FBQ0gsQ0FGRDtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLFNBQVM2QixXQUFULENBQXFCQyxLQUFyQixFQUE0QkMsR0FBNUIsRUFBaUM7QUFDN0IsTUFBSUMsQ0FBQyxHQUFHLElBQUlDLElBQUosQ0FBVUgsS0FBVixFQUFrQkksT0FBbEIsRUFBUjtBQUNBLE1BQUlDLENBQUMsR0FBRyxDQUFDSixHQUFHLEdBQUcsSUFBSUUsSUFBSixDQUFTRixHQUFULENBQUgsR0FBbUIsSUFBSUUsSUFBSixFQUF2QixFQUFtQ0MsT0FBbkMsRUFBUjtBQUNBLFNBQU9QLElBQUksQ0FBQ0ssQ0FBRCxFQUFHRyxDQUFILENBQVg7QUFDSDtBQUVEO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxTQUFTQyxXQUFULENBQXFCQyxpQkFBckIsRUFBd0M7QUFDcEMsTUFBSWhDLFVBQVUsR0FBRyxFQUFqQjtBQUNBLE1BQUlpQyxPQUFPLEdBQU0sWUFBakI7O0FBQ0EsT0FBSSxJQUFJQyxDQUFDLEdBQUcsQ0FBWixFQUFlQSxDQUFDLEdBQUdGLGlCQUFuQixFQUFzQ0UsQ0FBQyxFQUF2QyxFQUEwQztBQUN0QyxVQUFNQyxHQUFHLEdBQUdGLE9BQU8sQ0FBQ0csTUFBUixDQUFlRixDQUFmLENBQVo7QUFDQWxDLGNBQVUsQ0FBRW1DLEdBQUYsQ0FBVixHQUFvQixFQUFwQjtBQUNBLFFBQUlFLEtBQUssR0FBR2YsSUFBSSxDQUFDLENBQUQsRUFBSSxDQUFKLENBQWhCOztBQUNBLFNBQUksSUFBSWdCLENBQUMsR0FBRyxDQUFaLEVBQWVBLENBQUMsR0FBR0QsS0FBbkIsRUFBMEJDLENBQUMsRUFBM0IsRUFBK0I7QUFDM0J0QyxnQkFBVSxDQUFDbUMsR0FBRCxDQUFWLENBQWdCSSxJQUFoQixDQUFzQixNQUFLTCxDQUFFLEdBQUVJLENBQUUsRUFBakM7QUFDSDtBQUNKOztBQUNELFNBQU90QyxVQUFQO0FBQ0g7QUFHRDtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsTUFBTXdDLFdBQVcsR0FBRyxVQUFTQyxFQUFULEVBQ3BCO0FBQ0k7QUFDQSxRQUFNQyxLQUFLLEdBQUd2QyxRQUFRLENBQUNDLGdCQUFULENBQTBCLGdCQUExQixDQUFkO0FBQ0FzQyxPQUFLLENBQUNDLE9BQU4sQ0FBZUMsSUFBSSxJQUFJO0FBQ25CLFVBQU1DLEdBQUcsR0FBR0QsSUFBSSxDQUFDRSxTQUFqQjtBQUNBRCxPQUFHLENBQUNFLEdBQUosQ0FBUSxRQUFSO0FBQ0EsUUFBR0YsR0FBRyxDQUFDRyxRQUFKLENBQWEsVUFBYixDQUFILEVBQTZCSCxHQUFHLENBQUNJLE1BQUosQ0FBVyxVQUFYO0FBQ2hDLEdBSkQ7QUFLQSxRQUFNQyxhQUFhLEdBQUcvQyxRQUFRLENBQUNnRCxhQUFULENBQXdCLHVCQUFzQlYsRUFBRyxFQUFqRCxDQUF0QjtBQUNBUyxlQUFhLENBQUNKLFNBQWQsQ0FBd0JHLE1BQXhCLENBQStCLFFBQS9CO0FBQ0FDLGVBQWEsQ0FBQ0osU0FBZCxDQUF3QkMsR0FBeEIsQ0FBNEIsVUFBNUIsRUFWSixDQVdJOztBQUNBLFFBQU1LLE9BQU8sR0FBR2pELFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsc0JBQTFCLENBQWhCO0FBQ0FnRCxTQUFPLENBQUNULE9BQVIsQ0FBZ0JVLE1BQU0sSUFBSTtBQUN0QkEsVUFBTSxDQUFDUCxTQUFQLENBQWlCQyxHQUFqQixDQUFxQixVQUFyQjtBQUNILEdBRkQ7QUFHQSxRQUFNTyxlQUFlLEdBQUduRCxRQUFRLENBQUNnRCxhQUFULENBQXdCLDZCQUE0QlYsRUFBRyxFQUF2RCxDQUF4Qjs7QUFDQSxNQUFHYSxlQUFILEVBQW1CO0FBQ2ZBLG1CQUFlLENBQUNSLFNBQWhCLENBQTBCRyxNQUExQixDQUFpQyxVQUFqQztBQUNBcEQsT0FBRyxDQUFDMEQsS0FBSixDQUFXekQsWUFBWSxDQUFDMEQsTUFBYixDQUFvQnRCLENBQUMsSUFBSUEsQ0FBQyxDQUFDdUIsRUFBRixJQUFRaEIsRUFBakMsRUFBcUMsQ0FBckMsRUFBd0NpQixNQUFuRDtBQUNILEdBcEJMLENBcUJJOzs7QUFDQTNELGVBQWEsR0FBRyxJQUFoQjtBQUNILENBeEJEO0FBMEJBO0FBQ0E7QUFDQTs7O0FBQ0EsTUFBTTRELHVCQUF1QixHQUFHLFlBQ2hDO0FBQ0ksTUFBSWpCLEtBQUssR0FBR3ZDLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsZ0JBQTFCLENBQVo7QUFDQXNDLE9BQUssQ0FBQ0MsT0FBTixDQUFlQyxJQUFJLElBQUk7QUFDbkJBLFFBQUksQ0FBQ0UsU0FBTCxDQUFlRyxNQUFmLENBQXNCLFFBQXRCO0FBQ0FMLFFBQUksQ0FBQ0UsU0FBTCxDQUFlRyxNQUFmLENBQXNCLFVBQXRCO0FBQ0gsR0FIRDtBQUlBLFFBQU1HLE9BQU8sR0FBR2pELFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsc0JBQTFCLENBQWhCO0FBQ0FnRCxTQUFPLENBQUNULE9BQVIsQ0FBZ0JVLE1BQU0sSUFBSTtBQUN0QkEsVUFBTSxDQUFDUCxTQUFQLENBQWlCRyxNQUFqQixDQUF3QixVQUF4QjtBQUNILEdBRkQ7QUFHQWxELGVBQWEsR0FBRyxLQUFoQjtBQUNBRyxRQUFNLENBQUN5QyxPQUFQLENBQWdCLFVBQVNpQixLQUFULEVBQWU7QUFDM0JBLFNBQUssQ0FBQ0MsS0FBTjtBQUNILEdBRkQ7QUFHSCxDQWZEO0FBaUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBLE1BQU1DLFNBQVMsR0FBRyxVQUFTQyxPQUFULEVBQWlCO0FBQy9CLFFBQU0xQixLQUFLLEdBQUcsRUFBZDtBQUNBLFNBQU8wQixPQUFPLElBQUkxQixLQUFYLEdBQW1CQSxLQUFLLENBQUMwQixPQUFELENBQXhCLEdBQW9DQSxPQUEzQztBQUNILENBSEQ7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxNQUFNQyxRQUFRLEdBQUcsVUFBU0MsS0FBVCxFQUNqQjtBQUNJO0FBQ0FwRSxLQUFHLEdBQUdxRSw4Q0FBQyxDQUFDckUsR0FBRixDQUFNLEtBQU4sRUFBYztBQUNoQnNFLG1CQUFlLEVBQUcsQ0FBQ2hGLFNBREg7QUFDZTtBQUMvQmlGLGVBQVcsRUFBRyxLQUZFO0FBR2hCQyxXQUFPLEVBQUUsRUFITztBQUloQkMsV0FBTyxFQUFFO0FBSk8sR0FBZCxFQUtIQyxPQUxHLENBTUYsR0FBRzlELFlBQVksQ0FBQ0MsSUFOZCxDQUFOLENBRkosQ0FXSTs7QUFDQSxRQUFNOEQsUUFBUSxHQUFHL0QsWUFBWSxDQUFDRSxTQUFiLENBQ2JGLFlBQVksQ0FBQ00sZ0JBREEsQ0FBakIsQ0FaSixDQWdCSTs7QUFDQW1ELGdEQUFDLENBQUNPLFNBQUYsQ0FBYUQsUUFBUSxDQUFDM0QsS0FBdEIsRUFBNkI7QUFDekJDLGVBQVcsRUFBRTBELFFBQVEsQ0FBQzFEO0FBREcsR0FBN0IsRUFFRzRELEtBRkgsQ0FFUzdFLEdBRlQsRUFqQkosQ0FxQkk7O0FBQ0EsTUFBR29FLEtBQUgsRUFBUztBQUNMQSxTQUFLLEdBQUdBLEtBQUssQ0FBQ1QsTUFBTixDQUFjdEIsQ0FBQyxJQUFJQSxDQUFDLENBQUN3QixNQUFGLENBQVNpQixNQUFULEdBQWtCLENBQXJDLENBQVI7QUFDQSxVQUFNdkIsT0FBTyxHQUFHYSxLQUFLLENBQUNwRSxHQUFOLENBQVVxQyxDQUFDLElBQUk7QUFDM0IsVUFBSTBDLE9BQU8sR0FBSSxRQUFPMUMsQ0FBQyxDQUFDdUIsRUFBRyxFQUEzQjtBQUNBLFlBQU1vQixVQUFVLEdBQUssb0NBQW1DM0MsQ0FBQyxDQUFDNEMsS0FBTSxNQUFoRTtBQUNBLFVBQUlDLEtBQUssR0FBSSw4QkFBOEI3QyxDQUFDLENBQUM4QyxJQUFNLHFDQUFxQzlDLENBQUMsQ0FBQytDLE9BQVMsTUFBbkc7QUFDQS9DLE9BQUMsQ0FBQ0csS0FBRixDQUFRTSxPQUFSLENBQWlCdUMsSUFBSSxJQUFJO0FBQUVOLGVBQU8sSUFBSyxJQUFJTyw4Q0FBTyxDQUFDRCxJQUFELENBQVAsQ0FBY0UsV0FBZCxFQUE2QixFQUE3QztBQUFnRCxPQUEzRTtBQUNBLGFBQU9sQiw4Q0FBQyxDQUFDYixNQUFGLENBQVVuQixDQUFDLENBQUN3QixNQUFaLEVBQW9CO0FBQ3ZCMkIsWUFBSSxFQUFHbkIsOENBQUMsQ0FBQ29CLE9BQUYsQ0FBVTtBQUNiQyxrQkFBUSxFQUFPLENBQUUsRUFBRixFQUFNLEVBQU4sQ0FERjtBQUViQyxvQkFBVSxFQUFLLENBQUUsRUFBRixFQUFNLEVBQU4sQ0FGRjtBQUdiQyxzQkFBWSxFQUFHLENBQUUsRUFBRixFQUFNLENBQU4sQ0FIRjtBQUliQyxtQkFBUyxFQUFPLDRDQUpIO0FBS2JDLGNBQUksRUFBWSxnQ0FBK0JkLFVBQVcsWUFMN0M7QUFNYmUsaUJBQU8sRUFBUWYsVUFORjtBQU9iZ0IsbUJBQVMsRUFBTWpCO0FBUEYsU0FBVjtBQURnQixPQUFwQixFQVVKa0IsU0FWSSxDQVVPZixLQVZQLEVBVWVnQixFQVZmLENBVWtCLE9BVmxCLEVBVTJCLFVBQVNqRSxDQUFULEVBQVc7QUFDekNVLG1CQUFXLENBQUNOLENBQUMsQ0FBQ3VCLEVBQUgsQ0FBWDtBQUNILE9BWk0sQ0FBUDtBQWFILEtBbEJlLENBQWhCO0FBbUJBUyxrREFBQyxDQUFDOEIsVUFBRixDQUFjNUMsT0FBZCxFQUF3QnNCLEtBQXhCLENBQStCN0UsR0FBL0I7QUFDSDs7QUFFREEsS0FBRyxDQUFDa0csRUFBSixDQUFPLE9BQVAsRUFBaUJqRSxDQUFELElBQU07QUFDbEI2QiwyQkFBdUI7QUFDMUIsR0FGRDtBQUdILENBbEREO0FBb0RBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxNQUFNc0MsZUFBZSxHQUFHLFVBQVNoQyxLQUFULEVBQ3hCO0FBQ0ksTUFBRyxDQUFDQSxLQUFKLEVBQVc7QUFDWEEsT0FBSyxDQUFDdEIsT0FBTixDQUFlVCxDQUFDLElBQUk7QUFDbEIsVUFBTWdFLElBQUksR0FBRy9GLFFBQVEsQ0FBQ2dELGFBQVQsQ0FBd0IsdUJBQXVCakIsQ0FBQyxDQUFDdUIsRUFBSSxFQUFyRCxDQUFiO0FBQ0F5QyxRQUFJLENBQUMvQyxhQUFMLENBQW1CLHNCQUFuQixFQUEyQ2dELFNBQTNDLEdBQXVEdEgsS0FBSyxDQUFDcUQsQ0FBQyxDQUFDa0UsSUFBSCxDQUFMLENBQWNDLE9BQWQsRUFBdkQ7QUFDRCxHQUhEO0FBSUgsQ0FQRDs7QUFTQSxNQUFNQyxzQkFBc0IsR0FBRyxVQUFTSixJQUFULEVBQWM7QUFDekMsUUFBTUssUUFBUSxHQUFHTCxJQUFJLENBQUNNLFVBQUwsQ0FBZ0JDLE9BQWhCLENBQXdCdEUsR0FBekM7QUFDQSxRQUFNdUUsY0FBYyxHQUFHUixJQUFJLENBQUNNLFVBQUwsQ0FBZ0JwRyxnQkFBaEIsQ0FBaUMsd0JBQWpDLENBQXZCOztBQUNBLE1BQUc4RixJQUFJLENBQUNPLE9BQUwsQ0FBYUUsTUFBYixJQUF1QixnQkFBMUIsRUFBMkM7QUFDdkMsU0FBSSxNQUFNQyxDQUFWLElBQWU1RyxVQUFmLEVBQTBCO0FBQ3RCLFVBQUltQyxHQUFHLEdBQUduQyxVQUFVLENBQUM0RyxDQUFELENBQXBCOztBQUNBLFdBQUksTUFBTTFCLElBQVYsSUFBa0IvQyxHQUFHLENBQUNFLEtBQXRCLEVBQ0lGLEdBQUcsQ0FBQ0UsS0FBSixDQUFVNkMsSUFBVixJQUFrQixJQUFsQjs7QUFDSi9DLFNBQUcsQ0FBQzBFLFFBQUosR0FBZTFHLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMkIsY0FBYXdHLENBQUUsMkJBQTFDLEVBQXNFakMsTUFBckY7QUFDSDs7QUFDRHhFLFlBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsd0JBQTFCLEVBQW9EdUMsT0FBcEQsQ0FBNkRULENBQUMsSUFBSTtBQUM5REEsT0FBQyxDQUFDWSxTQUFGLENBQVlDLEdBQVosQ0FBZ0IsSUFBaEI7QUFDSCxLQUZEO0FBR0E1QyxZQUFRLENBQUNDLGdCQUFULENBQTBCLHNCQUExQixFQUFrRHVDLE9BQWxELENBQTJEVCxDQUFDLElBQUk7QUFDNURBLE9BQUMsQ0FBQ1ksU0FBRixDQUFZRyxNQUFaLENBQW1CLFFBQW5CO0FBQ0gsS0FGRDtBQUdBOUMsWUFBUSxDQUFDQyxnQkFBVCxDQUEwQixnQkFBMUIsRUFBNEN1QyxPQUE1QyxDQUFxRFQsQ0FBQyxJQUFJO0FBQ3REQSxPQUFDLENBQUNZLFNBQUYsQ0FBWUcsTUFBWixDQUFtQixRQUFuQjtBQUNILEtBRkQ7QUFHSDs7QUFBQTs7QUFDRCxNQUFHaUQsSUFBSSxDQUFDTyxPQUFMLENBQWFoRSxFQUFoQixFQUNBO0FBQ0ksVUFBTXNCLE9BQU8sR0FBR21DLElBQUksQ0FBQ08sT0FBTCxDQUFhaEUsRUFBN0IsQ0FESixDQUVJOztBQUNBLFFBQUd6QyxVQUFVLENBQUN1RyxRQUFELENBQVYsQ0FBcUJNLFFBQXJCLElBQWlDSCxjQUFjLENBQUMvQixNQUFuRCxFQUEwRDtBQUN0RCxXQUFJLE1BQU1PLElBQVYsSUFBa0JsRixVQUFVLENBQUN1RyxRQUFELENBQVYsQ0FBcUJsRSxLQUF2QyxFQUNJckMsVUFBVSxDQUFDdUcsUUFBRCxDQUFWLENBQXFCbEUsS0FBckIsQ0FBMkI2QyxJQUEzQixJQUFtQyxLQUFuQzs7QUFDSmxGLGdCQUFVLENBQUN1RyxRQUFELENBQVYsQ0FBcUJsRSxLQUFyQixDQUEyQjBCLE9BQTNCLElBQXNDLElBQXRDO0FBQ0EyQyxvQkFBYyxDQUFDL0QsT0FBZixDQUF3QlQsQ0FBQyxJQUFJO0FBQ3pCQSxTQUFDLENBQUNZLFNBQUYsQ0FBWUcsTUFBWixDQUFtQixJQUFuQjtBQUNILE9BRkQ7QUFHQWpELGdCQUFVLENBQUN1RyxRQUFELENBQVYsQ0FBcUJNLFFBQXJCLEdBQWdDLENBQWhDO0FBQ0FYLFVBQUksQ0FBQ3BELFNBQUwsQ0FBZUMsR0FBZixDQUFtQixJQUFuQjtBQUNILEtBVEQsTUFTTztBQUNIL0MsZ0JBQVUsQ0FBQ3VHLFFBQUQsQ0FBVixDQUFxQmxFLEtBQXJCLENBQTJCMEIsT0FBM0IsSUFBc0MsQ0FBQy9ELFVBQVUsQ0FBQ3VHLFFBQUQsQ0FBVixDQUFxQmxFLEtBQXJCLENBQTJCMEIsT0FBM0IsQ0FBdkM7QUFDQW1DLFVBQUksQ0FBQ3BELFNBQUwsQ0FBZWdFLE1BQWYsQ0FBc0IsSUFBdEI7QUFDQSxVQUFHOUcsVUFBVSxDQUFDdUcsUUFBRCxDQUFWLENBQXFCbEUsS0FBckIsQ0FBMkIwQixPQUEzQixDQUFILEVBQ0kvRCxVQUFVLENBQUN1RyxRQUFELENBQVYsQ0FBcUJNLFFBQXJCLEdBREosS0FHSTdHLFVBQVUsQ0FBQ3VHLFFBQUQsQ0FBVixDQUFxQk0sUUFBckI7QUFDUDs7QUFDRCxRQUFHN0csVUFBVSxDQUFDdUcsUUFBRCxDQUFWLENBQXFCTSxRQUFyQixJQUFpQyxDQUFwQyxFQUFzQztBQUNsQyxXQUFJLE1BQU0zQixJQUFWLElBQWtCbEYsVUFBVSxDQUFDdUcsUUFBRCxDQUFWLENBQXFCbEUsS0FBdkMsRUFDSXJDLFVBQVUsQ0FBQ3VHLFFBQUQsQ0FBVixDQUFxQmxFLEtBQXJCLENBQTJCNkMsSUFBM0IsSUFBbUMsSUFBbkM7O0FBQ0p3QixvQkFBYyxDQUFDL0QsT0FBZixDQUF3QlQsQ0FBQyxJQUFJO0FBQUVBLFNBQUMsQ0FBQ1ksU0FBRixDQUFZQyxHQUFaLENBQWdCLElBQWhCO0FBQXdCLE9BQXZEO0FBQ0EvQyxnQkFBVSxDQUFDdUcsUUFBRCxDQUFWLENBQXFCTSxRQUFyQixHQUFnQ0gsY0FBYyxDQUFDL0IsTUFBL0M7QUFDSDs7QUFDRG9DLGVBQVc7QUFDZDtBQUNKLENBakREO0FBbURBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxNQUFNQyxXQUFXLEdBQUcsVUFBUy9DLEtBQVQsRUFDcEI7QUFDSTtBQUNBQSxPQUFLLENBQUN0QixPQUFOLENBQWV1RCxJQUFJLElBQUk7QUFDbkIsU0FBSSxNQUFNL0QsR0FBVixJQUFpQitELElBQUksQ0FBQ2UsSUFBdEIsRUFDQTtBQUNJLFlBQU01RSxLQUFLLEdBQUc2RCxJQUFJLENBQUNlLElBQUwsQ0FBVTlFLEdBQVYsQ0FBZCxDQURKLENBRUk7O0FBQ0EsVUFBR0EsR0FBRyxJQUFJbkMsVUFBVixFQUFxQjtBQUNqQnFDLGFBQUssQ0FBQ00sT0FBTixDQUFjdUMsSUFBSSxJQUFJO0FBQ2xCLGNBQUksRUFBRUEsSUFBSSxJQUFJbEYsVUFBVSxDQUFDbUMsR0FBRCxDQUFWLENBQWdCRSxLQUExQixDQUFKLEVBQXNDO0FBQ2xDckMsc0JBQVUsQ0FBQ21DLEdBQUQsQ0FBVixDQUFnQkUsS0FBaEIsQ0FBc0I2QyxJQUF0QixJQUE4QixJQUE5QjtBQUNIO0FBQ0osU0FKRCxFQURpQixDQU1yQjtBQUNDLE9BUEQsTUFPTztBQUNIbEYsa0JBQVUsQ0FBQ21DLEdBQUQsQ0FBVixHQUFrQjtBQUNkLG1CQUFVO0FBREksU0FBbEI7QUFHQUUsYUFBSyxDQUFDTSxPQUFOLENBQWN1QyxJQUFJLElBQUk7QUFDbEJsRixvQkFBVSxDQUFDbUMsR0FBRCxDQUFWLENBQWdCRSxLQUFoQixDQUFzQjZDLElBQXRCLElBQThCLElBQTlCO0FBQ0gsU0FGRDtBQUdIOztBQUNEbEYsZ0JBQVUsQ0FBQ21DLEdBQUQsQ0FBVixDQUFnQjBFLFFBQWhCLEdBQTJCSyxNQUFNLENBQUNDLElBQVAsQ0FBWW5ILFVBQVUsQ0FBQ21DLEdBQUQsQ0FBVixDQUFnQkUsS0FBNUIsRUFBbUNzQyxNQUE5RDtBQUNIOztBQUFBO0FBQ0osR0F0QkQ7QUF3QkEsUUFBTXlDLGVBQWUsR0FBR2pILFFBQVEsQ0FBQ2dELGFBQVQsQ0FBdUIscUJBQXZCLENBQXhCO0FBRUEsTUFBSWtFLGFBQWEsR0FBRyxLQUFwQjs7QUFDQSxPQUFJLE1BQU1sRixHQUFWLElBQWlCbkMsVUFBakIsRUFBNEI7QUFDeEI7QUFDQSxRQUFJcUMsS0FBSyxHQUFHLEVBQVo7QUFDQSxVQUFNaUYsTUFBTSxHQUFHSixNQUFNLENBQUNDLElBQVAsQ0FBYW5ILFVBQVUsQ0FBQ21DLEdBQUQsQ0FBVixDQUFnQkUsS0FBN0IsRUFBcUNrRixJQUFyQyxFQUFmLENBSHdCLENBS3hCOztBQUNBLFFBQUl0SCxjQUFjLENBQUMwRSxNQUFmLEdBQXdCLENBQTVCLEVBQStCO0FBQzNCMUUsb0JBQWMsQ0FBQzBDLE9BQWYsQ0FBd0JSLEdBQUcsSUFBSTtBQUMzQixjQUFNcUYsR0FBRyxHQUFHRixNQUFNLENBQUM5RyxPQUFQLENBQWUyQixHQUFmLENBQVo7QUFDQSxZQUFHcUYsR0FBRyxHQUFHLENBQUMsQ0FBVixFQUFhRixNQUFNLENBQUMvRSxJQUFQLENBQWErRSxNQUFNLENBQUNHLE1BQVAsQ0FBY0QsR0FBZCxFQUFtQixDQUFuQixFQUFzQixDQUF0QixDQUFiO0FBQ2hCLE9BSEQ7QUFJSDs7QUFDRCxRQUFJRixNQUFNLENBQUMzQyxNQUFQLEdBQWdCLENBQXBCLEVBQXNCO0FBQ2xCMEMsbUJBQWEsR0FBRyxJQUFoQjtBQUNBQyxZQUFNLENBQUMzRSxPQUFQLENBQWdCLFVBQVN1QyxJQUFULEVBQWM7QUFDMUIsWUFBR0EsSUFBSCxFQUFTO0FBQ0w3QyxlQUFLLElBQUs7QUFDOUIsb0VBQXFFOEMsOENBQU8sQ0FBQ0QsSUFBSSxDQUFDRSxXQUFMLEVBQUQsQ0FBdUI7QUFDbkcsMEJBQTBCRixJQUFLO0FBQy9CLDBCQUhvQjtBQUlIO0FBQ0osT0FQRDtBQVFIOztBQUNELFFBQUc3QyxLQUFILEVBQVM7QUFDTCxVQUFJcUYsYUFBYSxHQUFJO0FBQ2pDO0FBQ0EsK0RBQStEdkYsR0FBSTtBQUNuRSxzQkFBc0JFLEtBQU07QUFDNUI7QUFDQSxtQkFMWTtBQU1BK0UscUJBQWUsQ0FBQ2pCLFNBQWhCLElBQTZCdUIsYUFBN0I7QUFDSDtBQUNKOztBQUNELE1BQUdMLGFBQUgsRUFBa0JELGVBQWUsQ0FBQ2pCLFNBQWhCLElBQThCO0FBQ3BEO0FBQ0EsY0FBZTlGLElBQUksSUFBSSxJQUFSLEdBQWUsZ0JBQWYsR0FBa0MsZ0JBQWtCO0FBQ25FLGFBSHNCO0FBTWxCK0csaUJBQWUsQ0FBQ08sZ0JBQWhCLENBQWlDLE9BQWpDLEVBQTBDN0YsQ0FBQyxJQUFJO0FBQzNDQSxLQUFDLENBQUM4RixlQUFGO0FBQ0F0QiwwQkFBc0IsQ0FBQ3hFLENBQUMsQ0FBQytGLE1BQUgsQ0FBdEI7QUFDSCxHQUhEO0FBS0EsTUFBSUMsTUFBTSxHQUFHM0gsUUFBUSxDQUFDZ0QsYUFBVCxDQUF1QixTQUF2QixDQUFiO0FBQ0FoRCxVQUFRLENBQUNnRCxhQUFULENBQXVCLGdCQUF2QixFQUF5Q3dFLGdCQUF6QyxDQUEwRCxPQUExRCxFQUFtRSxZQUFVO0FBQ3pFRyxVQUFNLENBQUNoRixTQUFQLENBQWlCZ0UsTUFBakIsQ0FBd0IsV0FBeEI7QUFDSCxHQUZEOztBQUdBLE1BQUczSCxTQUFILEVBQWE7QUFDVDJJLFVBQU0sQ0FBQ2hGLFNBQVAsQ0FBaUJDLEdBQWpCLENBQXFCLFdBQXJCO0FBQ0g7QUFDSixDQWpGRDtBQW1GQTtBQUNBO0FBQ0E7OztBQUNBLE1BQU1nRSxXQUFXLEdBQUcsWUFDcEI7QUFDSTtBQUNBLE1BQUlGLFFBQVEsR0FBRzFHLFFBQVEsQ0FBQ2dELGFBQVQsQ0FBdUIsV0FBdkIsQ0FBZjtBQUNBLE1BQUcwRCxRQUFILEVBQWFBLFFBQVEsQ0FBQy9ELFNBQVQsQ0FBbUJHLE1BQW5CLENBQTBCLFVBQTFCO0FBQ2JuRCxjQUFZLENBQUM2QyxPQUFiLENBQXNCdUQsSUFBSSxJQUMxQjtBQUNJLFFBQUk2QixhQUFhLEdBQUc1SCxRQUFRLENBQUNDLGdCQUFULENBQTJCLFNBQVE4RixJQUFJLENBQUN6QyxFQUFHLEVBQTNDLENBQXBCOztBQUNBLFNBQUksTUFBTThDLFFBQVYsSUFBc0J2RyxVQUF0QixFQUFpQztBQUM3QixVQUFJZ0ksVUFBVSxHQUFHLEtBQWpCO0FBQ0FELG1CQUFhLENBQUNwRixPQUFkLENBQXVCc0YsQ0FBQyxJQUFJO0FBQUVBLFNBQUMsQ0FBQ25GLFNBQUYsQ0FBWUcsTUFBWixDQUFtQixRQUFuQjtBQUE4QixPQUE1RDtBQUNBaUUsWUFBTSxDQUFDQyxJQUFQLENBQVluSCxVQUFVLENBQUN1RyxRQUFELENBQVYsQ0FBcUJsRSxLQUFqQyxFQUF3Q00sT0FBeEMsQ0FBZ0RGLEVBQUUsSUFDbEQ7QUFDSSxZQUFJekMsVUFBVSxDQUFDdUcsUUFBRCxDQUFWLENBQXFCbEUsS0FBckIsQ0FBMkJJLEVBQTNCLEtBQWtDeUQsSUFBSSxDQUFDN0QsS0FBTCxDQUFXNkYsUUFBWCxDQUFxQi9DLDhDQUFPLENBQUMxQyxFQUFELENBQVAsQ0FBWTJDLFdBQVosRUFBckIsQ0FBdEMsRUFBd0Y7QUFDcEY0QyxvQkFBVSxHQUFHLElBQWI7QUFDQTtBQUNIO0FBQ0osT0FORDs7QUFPQSxVQUFHLENBQUNBLFVBQUosRUFBZTtBQUNYRCxxQkFBYSxDQUFDcEYsT0FBZCxDQUF1QnNGLENBQUMsSUFBSTtBQUN4QkEsV0FBQyxDQUFDbkYsU0FBRixDQUFZQyxHQUFaLENBQWdCLFFBQWhCO0FBQ0gsU0FGRDtBQUdBO0FBQ0g7QUFDSjtBQUNKLEdBcEJEO0FBcUJILENBMUJEO0FBNEJBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSxNQUFNb0YsWUFBWSxHQUFHLFVBQVNsRSxLQUFULEVBQ3JCO0FBQ0ksU0FBT0EsS0FBSyxDQUFDcEUsR0FBTixDQUFVcUMsQ0FBQyxJQUFHO0FBQ2pCLFFBQUlHLEtBQUssR0FBRyxDQUFFSCxDQUFDLENBQUNxRSxRQUFKLENBQVo7QUFDQWxFLFNBQUssQ0FBQ0UsSUFBTixDQUFZLEdBQUdMLENBQUMsQ0FBQ2tHLGdCQUFqQjtBQUNBLFFBQUlDLFNBQVMsR0FBRyxDQUFDLEdBQUdoRyxLQUFKLENBQWhCO0FBQ0FnRyxhQUFTLENBQUM5RixJQUFWLENBQWdCTCxDQUFDLENBQUM0QyxLQUFGLENBQVFFLElBQXhCO0FBQ0FxRCxhQUFTLENBQUM5RixJQUFWLENBQWdCTCxDQUFDLENBQUNvRyxJQUFGLENBQU9DLFFBQVAsRUFBaEI7QUFDQSxXQUFPO0FBQ0g5RSxRQUFFLEVBQVF2QixDQUFDLENBQUN1QixFQURUO0FBRUh1QixVQUFJLEVBQU05QyxDQUFDLENBQUM4QyxJQUZUO0FBR0hGLFdBQUssRUFBSzVDLENBQUMsQ0FBQzRDLEtBQUYsQ0FBUXJDLEVBSGY7QUFJSHdDLGFBQU8sRUFBRy9DLENBQUMsQ0FBQytDLE9BSlQ7QUFLSGdDLFVBQUksRUFBSztBQUNMLG9CQUFhNUUsS0FEUjtBQUVMLGlCQUFhLENBQUVILENBQUMsQ0FBQzRDLEtBQUYsQ0FBUUUsSUFBVixDQUZSO0FBR0wsZ0JBQWEsQ0FBRTlDLENBQUMsQ0FBQ29HLElBQUo7QUFIUixPQUxOO0FBVUhFLGVBQVMsRUFBR3JELDhDQUFPLENBQUVqRCxDQUFDLENBQUNxRSxRQUFGLENBQVduQixXQUFYLEVBQUYsQ0FWaEI7QUFXSC9DLFdBQUssRUFBSWdHLFNBQVMsQ0FBQ3hJLEdBQVYsQ0FBZXlDLENBQUMsSUFBSTZDLDhDQUFPLENBQUU3QyxDQUFDLENBQUM4QyxXQUFGLEVBQUYsQ0FBM0IsQ0FYTjtBQVlIZ0IsVUFBSSxFQUFLbEUsQ0FBQyxDQUFDdUcsYUFaUjtBQWFIL0UsWUFBTSxFQUFHeEIsQ0FBQyxDQUFDd0csUUFBRixHQUFheEcsQ0FBQyxDQUFDd0csUUFBRixDQUFXQyxXQUFYLENBQXVCQyxPQUF2QixFQUFiLEdBQWdELEVBYnREO0FBY0hoRixXQUFLLEVBQUkxQixDQUFDLENBQUMwQjtBQWRSLEtBQVA7QUFnQkgsR0F0Qk0sQ0FBUDtBQXVCSCxDQXpCRDtBQTRCQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EsTUFBTWlGLFNBQVMsR0FBRyxVQUFTQyxRQUFULEVBQ2xCO0FBQ0ksTUFBSTdFLEtBQUssR0FBRyxFQUFaOztBQUVBLE1BQUc2RSxRQUFILEVBQVk7QUFDUkMsU0FBSyxDQUFDRCxRQUFELENBQUwsQ0FDS0UsSUFETCxDQUNXQyxRQUFRLElBQUlBLFFBQVEsQ0FBQ0MsSUFBVCxFQUR2QixFQUVLRixJQUZMLENBRVdHLElBQUksSUFBSTtBQUNYbEYsV0FBSyxHQUFHa0UsWUFBWSxDQUFDZ0IsSUFBRCxDQUFwQjtBQUNBckosa0JBQVksR0FBR21FLEtBQWY7QUFDQUQsY0FBUSxDQUFFQyxLQUFGLENBQVI7QUFDQStDLGlCQUFXLENBQUUvQyxLQUFGLENBQVg7QUFDQWdDLHFCQUFlLENBQUVoQyxLQUFGLENBQWY7O0FBQ0EsVUFBRzdFLE1BQU0sQ0FBQ2tCLFFBQVAsQ0FBZ0I4SSxJQUFuQixFQUF5QjtBQUNyQixjQUFNQSxJQUFJLEdBQUdoSyxNQUFNLENBQUNrQixRQUFQLENBQWdCOEksSUFBaEIsQ0FBcUJDLFNBQXJCLENBQStCLENBQS9CLENBQWI7QUFDQTdHLG1CQUFXLENBQUM0RyxJQUFELENBQVg7QUFDSDs7QUFDRCxVQUFJRSxZQUFZLEdBQUVuSixRQUFRLENBQUNnRCxhQUFULENBQXVCLHdEQUF2QixDQUFsQjtBQUNBbUQsNEJBQXNCLENBQUVnRCxZQUFGLENBQXRCO0FBQ0gsS0FkTDtBQWVILEdBaEJELE1BZ0JPO0FBQ0gsVUFBTUMsVUFBVSxHQUFHLEVBQW5CO0FBQ0F0RixTQUFLLEdBQUcsSUFBSXVGLEtBQUosQ0FBVUQsVUFBVixFQUFzQkUsSUFBdEIsR0FBNkI1SixHQUE3QixDQUFrQyxDQUFDcUMsQ0FBRCxFQUFJd0gsQ0FBSixNQUFXO0FBQ2pEakcsUUFBRSxFQUFPaUcsQ0FEd0M7QUFFakQxRSxVQUFJLEVBQUt6RixLQUFLLENBQUNvSyxhQUFOLENBQXFCckksSUFBSSxDQUFDLENBQUQsRUFBSSxDQUFKLENBQXpCLEVBQ01zSSxPQUROLENBQ2UsS0FEZixFQUNzQmhELENBQUMsSUFBSUEsQ0FBQyxDQUFDaUQsV0FBRixFQUQzQixDQUZ3QztBQUdNO0FBQ3ZEQyxVQUFJLEVBQUt2SyxLQUFLLENBQUN3SyxpQkFBTixDQUF5QnpJLElBQUksQ0FBQyxDQUFELEVBQUksQ0FBSixDQUE3QixDQUp3QztBQUtqRDBJLFNBQUcsRUFBTSxrQ0FMd0M7QUFNakQvQyxVQUFJLEVBQUtsRixXQUFXLENBQUMsQ0FBRCxDQU42QjtBQU9qRHFFLFVBQUksRUFBSzVFLFdBQVcsQ0FBQyxZQUFELENBUDZCO0FBUWpEa0MsWUFBTSxFQUFHMUMsYUFBYSxDQUFFUCxZQUFZLENBQUNDLElBQWIsQ0FBa0IsQ0FBbEIsQ0FBRixDQVIyQjtBQVNqRHVKLFNBQUcsRUFBTTtBQVR3QyxLQUFYLENBQWxDLEVBVUoxQyxJQVZJLENBVUMsQ0FBQzJDLENBQUQsRUFBR0MsQ0FBSCxLQUFTQSxDQUFDLENBQUMvRCxJQUFGLEdBQVM4RCxDQUFDLENBQUM5RCxJQVZyQixDQUFSO0FBV0FuQyxTQUFLLENBQUN0QixPQUFOLENBQWNULENBQUMsSUFBSTtBQUNmQSxPQUFDLENBQUNrRSxJQUFGLEdBQVMsSUFBSXhFLElBQUosQ0FBU00sQ0FBQyxDQUFDa0UsSUFBWCxFQUFpQmdFLGtCQUFqQixDQUFvQyxPQUFwQyxDQUFUO0FBQ0FsSSxPQUFDLENBQUNHLEtBQUYsR0FBVSxFQUFWOztBQUNBLFdBQUksTUFBTUYsR0FBVixJQUFpQkQsQ0FBQyxDQUFDK0UsSUFBbkIsRUFBd0I7QUFDcEIvRSxTQUFDLENBQUMrRSxJQUFGLENBQU85RSxHQUFQLEVBQVlRLE9BQVosQ0FBb0J1QyxJQUFJLElBQUk7QUFDeEJoRCxXQUFDLENBQUNHLEtBQUYsQ0FBUUUsSUFBUixDQUFhMkMsSUFBYjtBQUNILFNBRkQ7QUFHSDs7QUFBQTtBQUNKLEtBUkQ7QUFTQXBGLGdCQUFZLEdBQUdtRSxLQUFmO0FBQ0FELFlBQVEsQ0FBRUMsS0FBRixDQUFSO0FBQ0FvRyxpQkFBYSxDQUFFcEcsS0FBRixDQUFiO0FBQ0ErQyxlQUFXLENBQUUvQyxLQUFGLENBQVg7QUFDSDtBQUNKLENBL0NEOztBQWlEQTdFLE1BQU0sQ0FBQ2tMLE1BQVAsR0FBZ0IsTUFBSTtBQUNoQnpCLFdBQVMsQ0FBRSw2QkFBNEJ4SSxJQUFLLEVBQW5DLENBQVQ7QUFDQSxRQUFNa0ssZ0JBQWdCLEdBQUdwSyxRQUFRLENBQUNDLGdCQUFULENBQTBCLHlCQUExQixDQUF6QjtBQUNBbUssa0JBQWdCLENBQUM1SCxPQUFqQixDQUEwQixVQUFTaUIsS0FBVCxFQUFlO0FBQ3JDLFFBQUlILEVBQUUsR0FBR0csS0FBSyxDQUFDNEMsVUFBTixDQUFpQkEsVUFBakIsQ0FBNEJDLE9BQTVCLENBQW9DaEQsRUFBN0M7QUFDQUcsU0FBSyxDQUFDK0QsZ0JBQU4sQ0FBdUIsTUFBdkIsRUFBK0IsVUFBUzdGLENBQVQsRUFBVztBQUN0Q1UsaUJBQVcsQ0FBQ2lCLEVBQUQsQ0FBWDtBQUNBdkQsWUFBTSxDQUFDeUMsT0FBUCxDQUFlLFVBQVNULENBQVQsRUFBVztBQUN0QixZQUFHQSxDQUFDLElBQUlKLENBQUMsQ0FBQytGLE1BQVYsRUFBa0I7QUFDZDNGLFdBQUMsQ0FBQzJCLEtBQUY7QUFDSDtBQUNKLE9BSkQ7QUFLSCxLQVBEO0FBUUgsR0FWRDtBQVdBMUQsVUFBUSxDQUFDQyxnQkFBVCxDQUEwQixzQkFBMUIsRUFBa0R1QyxPQUFsRCxDQUEyRCxVQUFTVCxDQUFULEVBQVc7QUFDbEVBLEtBQUMsQ0FBQ3lGLGdCQUFGLENBQW1CLE9BQW5CLEVBQTRCLFVBQVM3RixDQUFULEVBQVc7QUFDcENBLE9BQUMsQ0FBQzhGLGVBQUY7QUFDQWpFLDZCQUF1QjtBQUN6QixLQUhEO0FBSUgsR0FMRDtBQU1BeEQsVUFBUSxDQUFDZ0QsYUFBVCxDQUF1QixZQUF2QixFQUFxQ3dFLGdCQUFyQyxDQUFzRCxPQUF0RCxFQUErRCxVQUFTN0YsQ0FBVCxFQUFXO0FBQ3RFNkIsMkJBQXVCO0FBQzFCLEdBRkQ7QUFHQSxRQUFNNkcsVUFBVSxHQUFHckssUUFBUSxDQUFDZ0QsYUFBVCxDQUF1QixzQkFBdkIsQ0FBbkI7QUFDQSxNQUFHcUgsVUFBSCxFQUFlQSxVQUFVLENBQUM3QyxnQkFBWCxDQUE0QixRQUE1QixFQUFzQyxVQUFTN0YsQ0FBVCxFQUFXO0FBQzVELFFBQUk0QixNQUFNLEdBQUc4RyxVQUFVLENBQUNDLEtBQVgsQ0FBaUJDLEtBQWpCLENBQXVCLEdBQXZCLENBQWI7QUFDQTdLLE9BQUcsQ0FBQzBFLE9BQUosQ0FBWWIsTUFBWixFQUFvQixFQUFwQjtBQUNILEdBSGM7QUFJZnZELFVBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsZ0JBQTFCLEVBQTRDdUMsT0FBNUMsQ0FBcUQsVUFBU1QsQ0FBVCxFQUFXO0FBQzVEQSxLQUFDLENBQUN5RixnQkFBRixDQUFtQixPQUFuQixFQUE0QixVQUFTN0YsQ0FBVCxFQUFXO0FBQ3BDLFVBQUcsQ0FBQ0ksQ0FBQyxDQUFDWSxTQUFGLENBQVlFLFFBQVosQ0FBcUIsVUFBckIsQ0FBSixFQUFzQztBQUNuQ1IsbUJBQVcsQ0FBQ04sQ0FBQyxDQUFDdUUsT0FBRixDQUFVaEQsRUFBWCxDQUFYO0FBQ0Y7QUFDSCxLQUpEO0FBS0gsR0FORDtBQU9ILENBbkNELEMiLCJmaWxlIjoibWFwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gaW5zdGFsbCBhIEpTT05QIGNhbGxiYWNrIGZvciBjaHVuayBsb2FkaW5nXG4gXHRmdW5jdGlvbiB3ZWJwYWNrSnNvbnBDYWxsYmFjayhkYXRhKSB7XG4gXHRcdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG4gXHRcdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG4gXHRcdHZhciBleGVjdXRlTW9kdWxlcyA9IGRhdGFbMl07XG5cbiBcdFx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG4gXHRcdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuIFx0XHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwLCByZXNvbHZlcyA9IFtdO1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoaW5zdGFsbGVkQ2h1bmtzLCBjaHVua0lkKSAmJiBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcbiBcdFx0XHRcdHJlc29sdmVzLnB1c2goaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKTtcbiBcdFx0XHR9XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuIFx0XHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0gPSBtb3JlTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdGlmKHBhcmVudEpzb25wRnVuY3Rpb24pIHBhcmVudEpzb25wRnVuY3Rpb24oZGF0YSk7XG5cbiBcdFx0d2hpbGUocmVzb2x2ZXMubGVuZ3RoKSB7XG4gXHRcdFx0cmVzb2x2ZXMuc2hpZnQoKSgpO1xuIFx0XHR9XG5cbiBcdFx0Ly8gYWRkIGVudHJ5IG1vZHVsZXMgZnJvbSBsb2FkZWQgY2h1bmsgdG8gZGVmZXJyZWQgbGlzdFxuIFx0XHRkZWZlcnJlZE1vZHVsZXMucHVzaC5hcHBseShkZWZlcnJlZE1vZHVsZXMsIGV4ZWN1dGVNb2R1bGVzIHx8IFtdKTtcblxuIFx0XHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIGFsbCBjaHVua3MgcmVhZHlcbiBcdFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4gXHR9O1xuIFx0ZnVuY3Rpb24gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKSB7XG4gXHRcdHZhciByZXN1bHQ7XG4gXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZE1vZHVsZXMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHR2YXIgZGVmZXJyZWRNb2R1bGUgPSBkZWZlcnJlZE1vZHVsZXNbaV07XG4gXHRcdFx0dmFyIGZ1bGZpbGxlZCA9IHRydWU7XG4gXHRcdFx0Zm9yKHZhciBqID0gMTsgaiA8IGRlZmVycmVkTW9kdWxlLmxlbmd0aDsgaisrKSB7XG4gXHRcdFx0XHR2YXIgZGVwSWQgPSBkZWZlcnJlZE1vZHVsZVtqXTtcbiBcdFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tkZXBJZF0gIT09IDApIGZ1bGZpbGxlZCA9IGZhbHNlO1xuIFx0XHRcdH1cbiBcdFx0XHRpZihmdWxmaWxsZWQpIHtcbiBcdFx0XHRcdGRlZmVycmVkTW9kdWxlcy5zcGxpY2UoaS0tLCAxKTtcbiBcdFx0XHRcdHJlc3VsdCA9IF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gZGVmZXJyZWRNb2R1bGVbMF0pO1xuIFx0XHRcdH1cbiBcdFx0fVxuXG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9XG5cbiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHQvLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbiBcdC8vIFByb21pc2UgPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHRcIm1hcFwiOiAwXG4gXHR9O1xuXG4gXHR2YXIgZGVmZXJyZWRNb2R1bGVzID0gW107XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL1wiO1xuXG4gXHR2YXIganNvbnBBcnJheSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSB8fCBbXTtcbiBcdHZhciBvbGRKc29ucEZ1bmN0aW9uID0ganNvbnBBcnJheS5wdXNoLmJpbmQoanNvbnBBcnJheSk7XG4gXHRqc29ucEFycmF5LnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjaztcbiBcdGpzb25wQXJyYXkgPSBqc29ucEFycmF5LnNsaWNlKCk7XG4gXHRmb3IodmFyIGkgPSAwOyBpIDwganNvbnBBcnJheS5sZW5ndGg7IGkrKykgd2VicGFja0pzb25wQ2FsbGJhY2soanNvbnBBcnJheVtpXSk7XG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IG9sZEpzb25wRnVuY3Rpb247XG5cblxuIFx0Ly8gYWRkIGVudHJ5IG1vZHVsZSB0byBkZWZlcnJlZCBsaXN0XG4gXHRkZWZlcnJlZE1vZHVsZXMucHVzaChbXCIuL3NyYy9pbmRleC5qc1wiLFwidmVuZG9yc35tYXBcIl0pO1xuIFx0Ly8gcnVuIGRlZmVycmVkIG1vZHVsZXMgd2hlbiByZWFkeVxuIFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4iLCJpbXBvcnQgTCBmcm9tICdsZWFmbGV0JztcbmltcG9ydCB7IExvcmVtSXBzdW0gfSBmcm9tIFwibG9yZW0taXBzdW1cIjtcbmltcG9ydCBzbHVnaWZ5IGZyb20gXCJzbHVnaWZ5XCI7XG52YXIgZGF5anMgPSByZXF1aXJlKCdkYXlqcycpXG52YXIgcmVsYXRpdmVUaW1lID0gcmVxdWlyZSgnZGF5anMvcGx1Z2luL3JlbGF0aXZlVGltZScpXG52YXIgZGF5anNfZXMgPSByZXF1aXJlKCdkYXlqcy9sb2NhbGUvZXUnKTtcbmRheWpzLmV4dGVuZChyZWxhdGl2ZVRpbWUpO1xuZGF5anMubG9jYWxlKCdldScpO1xuXG4vLyBEZXRlY3QgaWYgZGV2aWNlIGlzIG1vYmlsZVxuLy8gQ2hlY2sgYnJlYWtwb2ludHMgaW4gc2FzcyBzbyB0aGlzIHZhbHVlIG1hdGNoZXMgYnJlYWtwb2ludHMgaW4gdGhlIGxheW91dFxubGV0IGlzX21vYmlsZSA9IHdpbmRvdy5tYXRjaE1lZGlhKFxuICAgIFwib25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KVwiXG4pLm1hdGNoZXM7XG5cbi8vIFNldHMgdXAgbG9yZW0gaXBzdW0gZ2VuZXJhdG9yXG5jb25zdCBsb3JlbSA9IG5ldyBMb3JlbUlwc3VtKHtcbiAgICBzZW50ZW5jZXNQZXJQYXJhZ3JhcGggOiB7IG1heDogIDgsIG1pbjogNCB9LFxuICAgIHdvcmRzUGVyU2VudGVuY2UgICAgICA6IHsgbWF4OiAxNiwgbWluOiA0IH1cbn0pO1xuXG5sZXQgbWFwO1xubGV0IGl0ZW1zX2xvb2t1cCAgPSBbXTtcbmxldCBpdGVtX3NlbGVjdGVkID0gZmFsc2U7XG5cbmxldCBjYXRlZ29yaWVzID0ge307XG4vLyBTcGVjaWZ5IHRoZSBuYW1lIG9mIHRoZSAnT3RoZXInIGNhdGVnb3J5IHRvIHB1dCBpdCBpbiB0aGUgbGFzdCBwb3NpdGlvbiBpbiB0aGUgbWVudVxuY29uc3QgY2F0ZWdvcnlfb3RoZXIgPSBbJ0JpYWsnLCAnQmVzdGUgYmF0enVrJ107XG5jb25zdCB2aWRlb3MgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCd2aWRlbycpO1xuY29uc3QgbGFuZyAgID0gd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lLmluZGV4T2YoJy9lcy8nKSA+IC0xID8gJ2VzJyA6ICdldSc7XG5cbi8vIExlYWZsZXQgbWFwIGRlZmF1bHRzXG5jb25zdCBNQVBfREVGQVVMVFMgPSB7XG4gICAgdmlldyA6IFsgWyA0My4zMjQ2MDMxLC0yLjk3Njk3OTYgXSwgMTQgXSxcbiAgICBwcm92aWRlcnMgOiB7XG4gICAgICAgIG9zbSA6IHtcbiAgICAgICAgICAgIHRpbGVzICAgICAgIDogJ2h0dHBzOi8ve3N9LnRpbGUub3BlbnN0cmVldG1hcC5vcmcve3p9L3t4fS97eX0ucG5nJyxcbiAgICAgICAgICAgIGF0dHJpYnV0aW9uIDogJyZjb3B5OyA8YSBocmVmPVwiaHR0cHM6Ly93d3cub3BlbnN0cmVldG1hcC5vcmcvY29weXJpZ2h0XCI+T3BlblN0cmVldE1hcDwvYT4gY29udHJpYnV0b3JzJyxcbiAgICAgICAgfSxcbiAgICB9LFxuICAgIGRlZmF1bHRfcHJvdmlkZXIgOiAnb3NtJyxcbn07IFxuXG4vKipcbiAqICBHZXRzIGEgcmFuZG9tIGxhdGxvbiBuZWFyIGEgZ2l2ZW4gcG9pbnRcbiAqICBAcGFyYW0ge0FycmF5fSBwb2ludCAtIENvb3JkcyBhcnJheSBpbiB0aGUgZm9ybSBbIGxhdGl0dWRlLCBsb25naXR1ZGUgXVxuICogIEBwYXJhbSB7RmxvYXR9IGRpc3RhbmNlX2ZhY3Rvcl9sYXQgLSBEaXN0YW5jZSBtdWx0aXBsaWVyIC1sYXRpdHVkZVxuICogIEBwYXJhbSB7RmxvYXR9IGRpc3RhbmNlX2ZhY3Rvcl9sb24gLSBEaXN0YW5jZSBtdWx0aXBsaWVyIC0gbG9uZ2l0dWRlXG4gKi9cbmNvbnN0IHJhbmRvbV9sYXRsb24gPSBmdW5jdGlvbihwb2ludCwgZGlzdGFuY2VfZmFjdG9yX2xhdD0uMjUsIGRpc3RhbmNlX2ZhY3Rvcl9sb249LjI1ICl7XG4gICAgcmV0dXJuIFtcbiAgICAgICAgcG9pbnRbMF0gKyAoTWF0aC5yYW5kb20oKSAtIC41KSAqIGRpc3RhbmNlX2ZhY3Rvcl9sYXQsXG4gICAgICAgIHBvaW50WzFdICsgKE1hdGgucmFuZG9tKCkgLSAuNSkgKiBkaXN0YW5jZV9mYWN0b3JfbG9uXG4gICAgXTtcbn1cblxuLyoqXG4gKiAgR2V0cyBhIHJhbmRvbSBpbnRlZ2VyXG4gKiAgQHBhcmFtIHtJbnRlZ2VyfSBtaW4gLSBNaW5pbXVtIHZhbHVlXG4gKiAgQHBhcmFtIHtJbnRlZ2VyfSBtYXggLSBNYXhpbXVtIHZhbHVlXG4gKi9cbmNvbnN0IHJpbnQgPSBmdW5jdGlvbihtaW4sIG1heCl7XG4gICAgcmV0dXJuIG1pbiArIE1hdGguZmxvb3IoIE1hdGgucmFuZG9tKCkgKiAobWF4LW1pbikgKTtcbn1cblxuLyoqXG4gKiAgR2V0cyBhIHJhbmRvbSBkYXRlXG4gKiAgQHBhcmFtIHtTdHJpbmd9IHN0YXJ0IC0gU3RhcnQgZGF0ZSBpbiB0aGUgZm9ybWF0IFwieXl5eS1kZC1tbVwiXG4gKiAgQHBhcmFtIHtTdHJpbmd9IGVuZCAgIC0gRW5kIGRhdGUgaW4gdGhlIGZvcm1hdCBcInl5eXktZGQtbW1cIlxuICovXG5mdW5jdGlvbiByYW5kb21fZGF0ZShzdGFydCwgZW5kKSB7XG4gICAgdmFyIHMgPSBuZXcgRGF0ZSggc3RhcnQgKS5nZXRUaW1lKCk7XG4gICAgdmFyIGUgPSAoZW5kID8gbmV3IERhdGUoZW5kKSA6IG5ldyBEYXRlKCkpLmdldFRpbWUoKTtcbiAgICByZXR1cm4gcmludChzLGUpO1xufVxuXG4vKipcbiAqICBHZXRzIHJhbmRvbSBjYXRlZ29yaWVzXG4gKiAgQHBhcmFtIHtJbnRlZ2VyfSBjYXRlZ29yaWVzX251bWJlciAtIE51bWJlciBvZiBpbmRlcGVuZGVudCBjYXRlZ29yaWVzIChub3QgdGVybXMpXG4gKi9cbmZ1bmN0aW9uIHJhbmRvbV9jYXRzKGNhdGVnb3JpZXNfbnVtYmVyKSB7XG4gICAgbGV0IGNhdGVnb3JpZXMgPSBbXTtcbiAgICBsZXQgbGV0dGVycyAgICA9ICdhYmNlZGVmaGlqJztcbiAgICBmb3IobGV0IGkgPSAwOyBpIDwgY2F0ZWdvcmllc19udW1iZXI7IGkrKyl7XG4gICAgICAgIGNvbnN0IGNhdCA9IGxldHRlcnMuY2hhckF0KGkpO1xuICAgICAgICBjYXRlZ29yaWVzWyBjYXQgXSA9IFtdO1xuICAgICAgICBsZXQgdGVybXMgPSByaW50KDEsIDYpO1xuICAgICAgICBmb3IobGV0IGogPSAwOyBqIDwgdGVybXM7IGorKykge1xuICAgICAgICAgICAgY2F0ZWdvcmllc1tjYXRdLnB1c2goYHQtLSR7aX0ke2p9YCk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGNhdGVnb3JpZXM7XG59XG5cblxuLyoqXG4gKiAgSGlkZXMgYWxsIHRpbWVsaW5lIGl0ZW1zIGV4Y2VwdCB0aGUgb25lIHNlbGVjdGVkXG4gKiAgQHBhcmFtIHtJbnRlZ2VyfSBpZCAtIGlkIG9mIHRoZSBzZWxlY3RlZCBpdGVtXG4gKi9cbmNvbnN0IHNlbGVjdF9pdGVtID0gZnVuY3Rpb24oaWQpXG57XG4gICAgLy8gVGltZWxpbmVcbiAgICBjb25zdCBwb3N0cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy50aW1lbGluZS1pdGVtJyk7XG4gICAgcG9zdHMuZm9yRWFjaCggcG9zdCA9PiB7XG4gICAgICAgIGNvbnN0IGNscyA9IHBvc3QuY2xhc3NMaXN0O1xuICAgICAgICBjbHMuYWRkKCdoaWRkZW4nKTtcbiAgICAgICAgaWYoY2xzLmNvbnRhaW5zKCdzZWxlY3RlZCcpKSBjbHMucmVtb3ZlKCdzZWxlY3RlZCcpO1xuICAgIH0pO1xuICAgIGNvbnN0IHNlbGVjdGVkX3Bvc3QgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAudGltZWxpbmUtaXRlbS5pdGVtLSR7aWR9YCk7XG4gICAgc2VsZWN0ZWRfcG9zdC5jbGFzc0xpc3QucmVtb3ZlKCdoaWRkZW4nKTtcbiAgICBzZWxlY3RlZF9wb3N0LmNsYXNzTGlzdC5hZGQoJ3NlbGVjdGVkJyk7XG4gICAgLy8gTWFwXG4gICAgY29uc3QgbWFya2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5sZWFmbGV0LW1hcmtlci1pY29uJyk7XG4gICAgbWFya2Vycy5mb3JFYWNoKG1hcmtlciA9PiB7XG4gICAgICAgIG1hcmtlci5jbGFzc0xpc3QuYWRkKCdpbmFjdGl2ZScpO1xuICAgIH0pO1xuICAgIGNvbnN0IHNlbGVjdGVkX21hcmtlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoYC5sZWFmbGV0LW1hcmtlci1pY29uLml0ZW0tJHtpZH1gKTtcbiAgICBpZihzZWxlY3RlZF9tYXJrZXIpe1xuICAgICAgICBzZWxlY3RlZF9tYXJrZXIuY2xhc3NMaXN0LnJlbW92ZSgnaW5hY3RpdmUnKTtcbiAgICAgICAgbWFwLnBhblRvKCBpdGVtc19sb29rdXAuZmlsdGVyKGkgPT4gaS5wayA9PSBpZClbMF0ubGF0bG9uICk7XG4gICAgfVxuICAgIC8vIEFsbFxuICAgIGl0ZW1fc2VsZWN0ZWQgPSB0cnVlO1xufVxuXG4vKipcbiAqICBTaG93cyBhbGwgaXRlbXMgaW4gdGltZWxpbmUgaWYgYW55b25lIGlzIHNlbGVjdGVkXG4gKi9cbmNvbnN0IHNob3dfYWxsX3RpbWVsaW5lX2l0ZW1zID0gZnVuY3Rpb24oKVxue1xuICAgIHZhciBwb3N0cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy50aW1lbGluZS1pdGVtJyk7XG4gICAgcG9zdHMuZm9yRWFjaCggcG9zdCA9PiB7XG4gICAgICAgIHBvc3QuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZGVuJyk7XG4gICAgICAgIHBvc3QuY2xhc3NMaXN0LnJlbW92ZSgnc2VsZWN0ZWQnKTtcbiAgICB9KTtcbiAgICBjb25zdCBtYXJrZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmxlYWZsZXQtbWFya2VyLWljb24nKTtcbiAgICBtYXJrZXJzLmZvckVhY2gobWFya2VyID0+IHtcbiAgICAgICAgbWFya2VyLmNsYXNzTGlzdC5yZW1vdmUoJ2luYWN0aXZlJyk7XG4gICAgfSk7XG4gICAgaXRlbV9zZWxlY3RlZCA9IGZhbHNlO1xuICAgIHZpZGVvcy5mb3JFYWNoKCBmdW5jdGlvbih2aWRlbyl7XG4gICAgICAgIHZpZGVvLnBhdXNlKCk7XG4gICAgfSk7XG59XG5cbi8qKlxuICogIFJldHVybnMgdGhlIGxhYmVsIG9mIGEgZ2l2ZW4gdGVybV9pZFxuICogIEBwYXJhbSB7U3RyaW5nfSBjYXRlZ29yeV9uYW1lIC0gTmFtZSBvZiB0aGUgY2F0ZWdvcnlcbiAqICBAcGFyYW0ge1N0cmluZ30gdGVybV9uYW1lIC0gTmFtZSBvZiB0aGUgdGVybVxuICovXG5jb25zdCB0ZXJtX25hbWUgPSBmdW5jdGlvbih0ZXJtX2lkKXtcbiAgICBjb25zdCB0ZXJtcyA9IHt9O1xuICAgIHJldHVybiB0ZXJtX2lkIGluIHRlcm1zID8gdGVybXNbdGVybV9pZF0gOiB0ZXJtX2lkO1xufVxuXG4vKipcbiAqICBTZXRzIHVwIGxlYWZsZXQgbWFwXG4gKiAgQHBhcmFtIHtNYXB9IG1hcCAgICAgLSBBIHZhcmlhYmxlIHRvIGhvbGQgdGhlIGxlYWZsZXQgbWFwIGFuZCBtYWtlIGl0IGFjY2VzaWJsZSB0byBvdGhlciBtZXRob2RzXG4gKiAgQHBhcmFtIHtBcnJheX0gaXRlbXMgLSBBcnJheSB3aXRoIGRhdGEgdG8gc2V0IG1hcmtlcnNcbiAqL1xuY29uc3QgaW5pdF9tYXAgPSBmdW5jdGlvbihpdGVtcylcbntcbiAgICAvLyBDcmVhdGUgbWFwXG4gICAgbWFwID0gTC5tYXAoJ21hcCcgLCB7XG4gICAgICAgIHNjcm9sbFdoZWVsWm9vbSA6ICFpc19tb2JpbGUsICAvLyBkaXNhYmxlIHdoZWVsem9vbSBvbmx5IGluIHNtYWxsIGRldmljZXNcbiAgICAgICAgem9vbUNvbnRyb2wgOiBmYWxzZSxcbiAgICAgICAgbWluWm9vbTogMTIsXG4gICAgICAgIG1heFpvb206IDE3XG4gICAgfSkuc2V0VmlldyhcbiAgICAgICAgLi4uTUFQX0RFRkFVTFRTLnZpZXdcbiAgICApO1xuXG4gICAgLy8gU2V0IHVwIHByb3ZpZGVyIGZyb20gZGVmYXVsdHNcbiAgICBjb25zdCBwcm92aWRlciA9IE1BUF9ERUZBVUxUUy5wcm92aWRlcnNbXG4gICAgICAgIE1BUF9ERUZBVUxUUy5kZWZhdWx0X3Byb3ZpZGVyXG4gICAgXTtcblxuICAgIC8vIFNldCB1cCB0aWxlc1xuICAgIEwudGlsZUxheWVyKCBwcm92aWRlci50aWxlcywge1xuICAgICAgICBhdHRyaWJ1dGlvbjogcHJvdmlkZXIuYXR0cmlidXRpb24sXG4gICAgfSkuYWRkVG8obWFwKTtcblxuICAgIC8vIExvYWQgaXRlbXNcbiAgICBpZihpdGVtcyl7XG4gICAgICAgIGl0ZW1zID0gaXRlbXMuZmlsdGVyKCBpID0+IGkubGF0bG9uLmxlbmd0aCA+IDApO1xuICAgICAgICBjb25zdCBtYXJrZXJzID0gaXRlbXMubWFwKGkgPT4ge1xuICAgICAgICAgICAgbGV0IGNsYXNzZXMgPSBgaXRlbS0ke2kucGt9YDtcbiAgICAgICAgICAgIGNvbnN0IG1hcmtlcl9pbWcgID0gYC9zdGF0aWMvc2l0ZS9pbWcvbWFya2Vycy9tYXJrZXItcyR7aS5zdGF0ZX0uc3ZnYDtcbiAgICAgICAgICAgIGxldCBwb3B1cCA9IGA8aDQgY2xhc3M9XCJtYXAtaXRlbV9fbmFtZVwiPiR7IGkubmFtZSB9PC9oND48cCBjbGFzcz1cIm1hcC1pdGVtX19zdW1tYXJ5XCI+JHsgaS5zdW1tYXJ5IH08L3A+YDtcbiAgICAgICAgICAgIGkudGVybXMuZm9yRWFjaCggdGVybSA9PiB7IGNsYXNzZXMgKz0gYCAkeyBzbHVnaWZ5KHRlcm0pLnRvTG93ZXJDYXNlKCkgfWAgfSk7XG4gICAgICAgICAgICByZXR1cm4gTC5tYXJrZXIoIGkubGF0bG9uLCB7XG4gICAgICAgICAgICAgICAgaWNvbiA6IEwuZGl2SWNvbih7XG4gICAgICAgICAgICAgICAgICAgIGljb25TaXplICAgICA6IFsgNDAsIDQ4IF0sXG4gICAgICAgICAgICAgICAgICAgIHNoYWRvd1NpemUgICA6IFsgNDAsIDM1IF0sXG4gICAgICAgICAgICAgICAgICAgIHNoYWRvd0FuY2hvciA6IFsgMjAsIDAgXSxcbiAgICAgICAgICAgICAgICAgICAgc2hhZG93VXJsICAgIDogYC9zdGF0aWMvc2l0ZS9pbWcvbWFya2Vycy9tYXJrZXItc2hhZG93LnN2Z2AsXG4gICAgICAgICAgICAgICAgICAgIGh0bWwgICAgICAgICA6IGA8ZGl2IGNsYXNzPVwiaW5uZXJcIj48aW1nIHNyYz1cIiR7bWFya2VyX2ltZ31cIiAvPjwvZGl2PmAsXG4gICAgICAgICAgICAgICAgICAgIGljb25VcmwgICAgICA6IG1hcmtlcl9pbWcsXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZSAgICA6IGNsYXNzZXMsXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0pLmJpbmRQb3B1cCggcG9wdXAgKS5vbignY2xpY2snLCBmdW5jdGlvbihlKXtcbiAgICAgICAgICAgICAgICBzZWxlY3RfaXRlbShpLnBrKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pO1xuICAgICAgICBMLmxheWVyR3JvdXAoIG1hcmtlcnMgKS5hZGRUbyggbWFwICk7XG4gICAgfVxuXG4gICAgbWFwLm9uKCdjbGljaycsIChlKT0+IHtcbiAgICAgICAgc2hvd19hbGxfdGltZWxpbmVfaXRlbXMoKTtcbiAgICB9KTtcbn1cblxuLyoqXG4gKiAgVXBkYXRlIHRpbWVsaW5lXG4gKiAgQHBhcmFtIHtBcnJheX0gaXRlbXMgLSBBcnJheSB3aXRoIGRhdGEgdG8gc2V0IGl0ZW1zIGluIHRoZSB0aW1lbGluZVxuICovXG5jb25zdCB1cGRhdGVfdGltZWxpbmUgPSBmdW5jdGlvbihpdGVtcylcbntcbiAgICBpZighaXRlbXMpIHJldHVybjtcbiAgICBpdGVtcy5mb3JFYWNoKCBpID0+IHtcbiAgICAgIGNvbnN0IGl0ZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAudGltZWxpbmUtaXRlbS5pdGVtLSR7IGkucGsgfWApO1xuICAgICAgaXRlbS5xdWVyeVNlbGVjdG9yKCcudGltZWxpbmUtaXRlbV9fZGF0ZScpLmlubmVySFRNTCA9IGRheWpzKGkuZGF0ZSkuZnJvbU5vdygpO1xuICAgIH0pO1xufVxuXG5jb25zdCBzZWxlY3RfbGVnZW5kX2NhdGVnb3J5ID0gZnVuY3Rpb24oaXRlbSl7XG4gICAgY29uc3QgY2F0ZWdvcnkgPSBpdGVtLnBhcmVudE5vZGUuZGF0YXNldC5jYXQ7XG4gICAgY29uc3QgY2F0ZWdvcnlfdGVybXMgPSBpdGVtLnBhcmVudE5vZGUucXVlcnlTZWxlY3RvckFsbCgnLmxlZ2VuZC1jYXRlZ29yeV9faXRlbScpO1xuICAgIGlmKGl0ZW0uZGF0YXNldC5hY3Rpb24gPT0gJ3JlbW92ZV9maWx0ZXJzJyl7XG4gICAgICAgIGZvcihjb25zdCBjIGluIGNhdGVnb3JpZXMpe1xuICAgICAgICAgICAgdmFyIGNhdCA9IGNhdGVnb3JpZXNbY107XG4gICAgICAgICAgICBmb3IoY29uc3QgdGVybSBpbiBjYXQudGVybXMpXG4gICAgICAgICAgICAgICAgY2F0LnRlcm1zW3Rlcm1dID0gdHJ1ZTtcbiAgICAgICAgICAgIGNhdC5zZWxlY3RlZCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYFtkYXRhLWNhdD0nJHtjfSddIC5sZWdlbmQtY2F0ZWdvcnlfX2l0ZW1gKS5sZW5ndGg7XG4gICAgICAgIH1cbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmxlZ2VuZC1jYXRlZ29yeV9faXRlbScpLmZvckVhY2goIGkgPT4ge1xuICAgICAgICAgICAgaS5jbGFzc0xpc3QuYWRkKCdvbicpO1xuICAgICAgICB9KTtcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmxlYWZsZXQtbWFya2VyLWljb24nKS5mb3JFYWNoKCBpID0+IHtcbiAgICAgICAgICAgIGkuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZGVuJyk7XG4gICAgICAgIH0pO1xuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcudGltZWxpbmUtaXRlbScpLmZvckVhY2goIGkgPT4ge1xuICAgICAgICAgICAgaS5jbGFzc0xpc3QucmVtb3ZlKCdoaWRkZW4nKTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICBpZihpdGVtLmRhdGFzZXQuaWQpXG4gICAge1xuICAgICAgICBjb25zdCB0ZXJtX2lkID0gaXRlbS5kYXRhc2V0LmlkO1xuICAgICAgICAvLyBJZiBmaXJzdCB0ZXJtIHNlbGVjdGVkLCB1bmNoZWNrIGFsbCBleGNlcHRpbmcgaXRzZWxmXG4gICAgICAgIGlmKGNhdGVnb3JpZXNbY2F0ZWdvcnldLnNlbGVjdGVkID09IGNhdGVnb3J5X3Rlcm1zLmxlbmd0aCl7XG4gICAgICAgICAgICBmb3IoY29uc3QgdGVybSBpbiBjYXRlZ29yaWVzW2NhdGVnb3J5XS50ZXJtcylcbiAgICAgICAgICAgICAgICBjYXRlZ29yaWVzW2NhdGVnb3J5XS50ZXJtc1t0ZXJtXSA9IGZhbHNlO1xuICAgICAgICAgICAgY2F0ZWdvcmllc1tjYXRlZ29yeV0udGVybXNbdGVybV9pZF0gPSB0cnVlO1xuICAgICAgICAgICAgY2F0ZWdvcnlfdGVybXMuZm9yRWFjaCggaSA9PiB7XG4gICAgICAgICAgICAgICAgaS5jbGFzc0xpc3QucmVtb3ZlKCdvbicpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIGNhdGVnb3JpZXNbY2F0ZWdvcnldLnNlbGVjdGVkID0gMTtcbiAgICAgICAgICAgIGl0ZW0uY2xhc3NMaXN0LmFkZCgnb24nKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNhdGVnb3JpZXNbY2F0ZWdvcnldLnRlcm1zW3Rlcm1faWRdID0gIWNhdGVnb3JpZXNbY2F0ZWdvcnldLnRlcm1zW3Rlcm1faWRdO1xuICAgICAgICAgICAgaXRlbS5jbGFzc0xpc3QudG9nZ2xlKCdvbicpO1xuICAgICAgICAgICAgaWYoY2F0ZWdvcmllc1tjYXRlZ29yeV0udGVybXNbdGVybV9pZF0pXG4gICAgICAgICAgICAgICAgY2F0ZWdvcmllc1tjYXRlZ29yeV0uc2VsZWN0ZWQrKztcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICBjYXRlZ29yaWVzW2NhdGVnb3J5XS5zZWxlY3RlZC0tO1xuICAgICAgICB9XG4gICAgICAgIGlmKGNhdGVnb3JpZXNbY2F0ZWdvcnldLnNlbGVjdGVkID09IDApe1xuICAgICAgICAgICAgZm9yKGNvbnN0IHRlcm0gaW4gY2F0ZWdvcmllc1tjYXRlZ29yeV0udGVybXMpXG4gICAgICAgICAgICAgICAgY2F0ZWdvcmllc1tjYXRlZ29yeV0udGVybXNbdGVybV0gPSB0cnVlO1xuICAgICAgICAgICAgY2F0ZWdvcnlfdGVybXMuZm9yRWFjaCggaSA9PiB7IGkuY2xhc3NMaXN0LmFkZCgnb24nKTsgfSlcbiAgICAgICAgICAgIGNhdGVnb3JpZXNbY2F0ZWdvcnldLnNlbGVjdGVkID0gY2F0ZWdvcnlfdGVybXMubGVuZ3RoO1xuICAgICAgICB9XG4gICAgICAgIGNoZWNrX2l0ZW1zKCk7XG4gICAgfVxufVxuXG4vKipcbiAqICBTZXRzIHVwIGxlZ2VuZFxuICogIEBwYXJhbSB7QXJyYXl9IGl0ZW1zIC0gQXJyYXkgd2l0aCBkYXRhIHRvIHNldCBpdGVtcyBpbiB0aGUgdGltZWxpbmVcbiAqL1xuY29uc3QgaW5pdF9sZWdlbmQgPSBmdW5jdGlvbihpdGVtcylcbntcbiAgICAvLyBJdGVyYXRlIG92ZXIgZWxlbWVudHMgZ3JhYmJpbmcgaXRzIGNhdGVnb3JpZXNcbiAgICBpdGVtcy5mb3JFYWNoKCBpdGVtID0+IHtcbiAgICAgICAgZm9yKGNvbnN0IGNhdCBpbiBpdGVtLmNhdHMpXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvbnN0IHRlcm1zID0gaXRlbS5jYXRzW2NhdF07XG4gICAgICAgICAgICAvLyBJZiBhbHJlYWR5IGNyZWF0ZWQgcHVzaCBvbmx5IG5ldyB0ZXJtcyBpbnRvIHRoZSBjYXRlZ29yeVxuICAgICAgICAgICAgaWYoY2F0IGluIGNhdGVnb3JpZXMpe1xuICAgICAgICAgICAgICAgIHRlcm1zLmZvckVhY2godGVybSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGlmKCAhKHRlcm0gaW4gY2F0ZWdvcmllc1tjYXRdLnRlcm1zKSApe1xuICAgICAgICAgICAgICAgICAgICAgICAgY2F0ZWdvcmllc1tjYXRdLnRlcm1zW3Rlcm1dID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgLy8gRWxzZSBwdXNoIGFsbCBpdGVtIHRlcm1zIGludG8gdGhlIGNhdGVnb3J5XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGNhdGVnb3JpZXNbY2F0XSA9IHtcbiAgICAgICAgICAgICAgICAgICAgJ3Rlcm1zJyA6IHt9LFxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgdGVybXMuZm9yRWFjaCh0ZXJtID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY2F0ZWdvcmllc1tjYXRdLnRlcm1zW3Rlcm1dID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhdGVnb3JpZXNbY2F0XS5zZWxlY3RlZCA9IE9iamVjdC5rZXlzKGNhdGVnb3JpZXNbY2F0XS50ZXJtcykubGVuZ3RoO1xuICAgICAgICB9O1xuICAgIH0pO1xuXG4gICAgY29uc3QgY2F0ZWdvcmllc19ub2RlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxlZ2VuZF9fY2F0ZWdvcmllcycpO1xuXG4gICAgbGV0IHJlbW92ZV9idXR0b24gPSBmYWxzZTtcbiAgICBmb3IoY29uc3QgY2F0IGluIGNhdGVnb3JpZXMpe1xuICAgICAgICAvLyBDcmVhdGUgbWVudSBpdGVtc1xuICAgICAgICBsZXQgdGVybXMgPSAnJztcbiAgICAgICAgY29uc3Qgc29ydGVkID0gT2JqZWN0LmtleXMoIGNhdGVnb3JpZXNbY2F0XS50ZXJtcyApLnNvcnQoKTtcblxuICAgICAgICAvLyBQdXQgaW4gbGFzdCBwb3NpdGlvbiAnT3RoZXInIGNhdGVnb3J5XG4gICAgICAgIGlmKCBjYXRlZ29yeV9vdGhlci5sZW5ndGggPiAwICl7XG4gICAgICAgICAgICBjYXRlZ29yeV9vdGhlci5mb3JFYWNoKCBjYXQgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IGluZCA9IHNvcnRlZC5pbmRleE9mKGNhdCk7XG4gICAgICAgICAgICAgICAgaWYoaW5kID4gLTEpIHNvcnRlZC5wdXNoKCBzb3J0ZWQuc3BsaWNlKGluZCwgMSlbMF0gKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGlmKCBzb3J0ZWQubGVuZ3RoID4gMSl7XG4gICAgICAgICAgICByZW1vdmVfYnV0dG9uID0gdHJ1ZTtcbiAgICAgICAgICAgIHNvcnRlZC5mb3JFYWNoKCBmdW5jdGlvbih0ZXJtKXsgXG4gICAgICAgICAgICAgICAgaWYodGVybSkge1xuICAgICAgICAgICAgICAgICAgICB0ZXJtcyArPSBgXFxcbiAgICAgICAgICAgICAgICAgICAgPGxpIGNsYXNzPVwibGVnZW5kLWNhdGVnb3J5X19pdGVtIG9uXCIgZGF0YS1pZD1cIiR7IHNsdWdpZnkodGVybS50b0xvd2VyQ2FzZSgpICkgfVwiPlxcXG4gICAgICAgICAgICAgICAgICAgICAgICAke3Rlcm19XFxcbiAgICAgICAgICAgICAgICAgICAgPC9saT5gO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGlmKHRlcm1zKXtcbiAgICAgICAgICAgIHZhciBjYXRlZ29yeV9ub2RlID0gYFxcXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibGVnZW5kLWNhdGVnb3J5XCI+XFxcbiAgICAgICAgICAgICAgICA8dWwgY2xhc3M9XCJsZWdlbmQtY2F0ZWdvcnlfX2l0ZW1zXCIgZGF0YS1jYXQ9XCIke2NhdH1cIj5cXFxuICAgICAgICAgICAgICAgICAgICAke3Rlcm1zfVxcXG4gICAgICAgICAgICAgICAgPC91bD5cXFxuICAgICAgICAgICAgPC9kaXY+YDtcbiAgICAgICAgICAgIGNhdGVnb3JpZXNfbm9kZS5pbm5lckhUTUwgKz0gY2F0ZWdvcnlfbm9kZTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpZihyZW1vdmVfYnV0dG9uKSBjYXRlZ29yaWVzX25vZGUuaW5uZXJIVE1MICs9IGBcXFxuICAgICAgICA8cCBjbGFzcz1cImxlZ2VuZC1jYXRlZ29yeV9fdW5maWx0ZXJcIiBkYXRhLWFjdGlvbj1cInJlbW92ZV9maWx0ZXJzXCI+XFxcbiAgICAgICAgICAgICR7IGxhbmcgPT0gJ2VzJyA/ICdRdWl0YXIgZmlsdHJvcycgOiAnRGF0dWFrIGV6YWJhdHUnIH1cXFxuICAgICAgICA8L3A+YFxuICAgIDtcblxuICAgIGNhdGVnb3JpZXNfbm9kZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGUgPT4ge1xuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICBzZWxlY3RfbGVnZW5kX2NhdGVnb3J5KGUudGFyZ2V0KTtcbiAgICB9KTtcblxuICAgIHZhciBsZWdlbmQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcubGVnZW5kJyk7XG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxlZ2VuZF9fbGFiZWwnKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCl7XG4gICAgICAgIGxlZ2VuZC5jbGFzc0xpc3QudG9nZ2xlKCdjb2xsYXBzZWQnKTtcbiAgICB9KTtcbiAgICBpZihpc19tb2JpbGUpe1xuICAgICAgICBsZWdlbmQuY2xhc3NMaXN0LmFkZCgnY29sbGFwc2VkJylcbiAgICB9XG59XG5cbi8qKlxuICogIENoZWNrcyBtYXJrZXJzIGFuZCBzZXRzIGl0cyB2aXNpYmlsaXR5IHJlZ2FyZGluZyBzZWxlY3RlZCBjYXRlZ29yaWVzXG4gKi9cbmNvbnN0IGNoZWNrX2l0ZW1zID0gZnVuY3Rpb24oKVxue1xuICAgIC8vIElmIHRoZXJlJ3MgYSBzZWxlY3RlZCB0aW1lbGluZSBpdGVtLCBkZXNlbGVjdCBpdFxuICAgIHZhciBzZWxlY3RlZCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zZWxlY3RlZCcpO1xuICAgIGlmKHNlbGVjdGVkKSBzZWxlY3RlZC5jbGFzc0xpc3QucmVtb3ZlKCdzZWxlY3RlZCcpO1xuICAgIGl0ZW1zX2xvb2t1cC5mb3JFYWNoKCBpdGVtID0+XG4gICAge1xuICAgICAgICB2YXIgaXRlbV9kaXNwbGF5cyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoYC5pdGVtLSR7aXRlbS5wa31gKTtcbiAgICAgICAgZm9yKGNvbnN0IGNhdGVnb3J5IGluIGNhdGVnb3JpZXMpe1xuICAgICAgICAgICAgdmFyIGlzX3Zpc2libGUgPSBmYWxzZTtcbiAgICAgICAgICAgIGl0ZW1fZGlzcGxheXMuZm9yRWFjaCggZCA9PiB7IGQuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZGVuJykgfSk7XG4gICAgICAgICAgICBPYmplY3Qua2V5cyhjYXRlZ29yaWVzW2NhdGVnb3J5XS50ZXJtcykuZm9yRWFjaChpZCA9PlxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGlmKCBjYXRlZ29yaWVzW2NhdGVnb3J5XS50ZXJtc1tpZF0gJiYgaXRlbS50ZXJtcy5pbmNsdWRlcyggc2x1Z2lmeShpZCkudG9Mb3dlckNhc2UoKSApICl7XG4gICAgICAgICAgICAgICAgICAgIGlzX3Zpc2libGUgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBpZighaXNfdmlzaWJsZSl7XG4gICAgICAgICAgICAgICAgaXRlbV9kaXNwbGF5cy5mb3JFYWNoKCBkID0+IHtcbiAgICAgICAgICAgICAgICAgICAgZC5jbGFzc0xpc3QuYWRkKCdoaWRkZW4nKVxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0pO1xufVxuXG4vKipcbiAqICBTZXRzIGRhdGEgaXRlbXMgaW4gcHJvcGVyIGZvcm1hdFxuICogIEBwYXJhbSB7QXJyYXl9IGl0ZW1zIC0gQXJyYXkgd2l0aCBkYXRhIHRvIHNldCBpdGVtc1xuICovXG5jb25zdCBjcmVhdGVfaXRlbXMgPSBmdW5jdGlvbihpdGVtcylcbntcbiAgICByZXR1cm4gaXRlbXMubWFwKGkgPT57XG4gICAgICAgIGxldCB0ZXJtcyA9IFsgaS5jYXRlZ29yeSBdO1xuICAgICAgICB0ZXJtcy5wdXNoKCAuLi5pLm90aGVyX2NhdGVnb3JpZXMgKTtcbiAgICAgICAgbGV0IGFsbF90ZXJtcyA9IFsuLi50ZXJtc107XG4gICAgICAgIGFsbF90ZXJtcy5wdXNoKCBpLnN0YXRlLm5hbWUgKTtcbiAgICAgICAgYWxsX3Rlcm1zLnB1c2goIGkueWVhci50b1N0cmluZygpICk7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBwayAgICAgIDogaS5wayxcbiAgICAgICAgICAgIG5hbWUgICAgOiBpLm5hbWUsXG4gICAgICAgICAgICBzdGF0ZSAgIDogaS5zdGF0ZS5pZCxcbiAgICAgICAgICAgIHN1bW1hcnkgOiBpLnN1bW1hcnksXG4gICAgICAgICAgICBjYXRzICAgOiB7IFxuICAgICAgICAgICAgICAgICdjYXRlZ29yeScgOiB0ZXJtcywgXG4gICAgICAgICAgICAgICAgJ3N0YXRlJyAgICA6IFsgaS5zdGF0ZS5uYW1lIF0sXG4gICAgICAgICAgICAgICAgJ3llYXInICAgICA6IFsgaS55ZWFyIF0sXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgbWFpbl90ZXJtIDogc2x1Z2lmeSggaS5jYXRlZ29yeS50b0xvd2VyQ2FzZSgpICksXG4gICAgICAgICAgICB0ZXJtcyAgOiBhbGxfdGVybXMubWFwKCBqID0+IHNsdWdpZnkoIGoudG9Mb3dlckNhc2UoKSApKSxcbiAgICAgICAgICAgIGRhdGUgICA6IGkuY3JlYXRpb25fZGF0ZSxcbiAgICAgICAgICAgIGxhdGxvbiA6IGkucG9zaXRpb24gPyBpLnBvc2l0aW9uLmNvb3JkaW5hdGVzLnJldmVyc2UoKSA6IFtdLFxuICAgICAgICAgICAgdmlkZW8gIDogaS52aWRlbyxcbiAgICAgICAgfVxuICAgIH0pO1xufVxuXG5cbi8qKlxuICogIExvYWRzIGRhdGEgZnJvbSBhIHJlbW90ZSBlbmRwb2ludFxuICogIEBwYXJhbSB7U3RyaW5nfSBlbmRwb2ludCAtIFVSTCBvZiBhbmQgZW5kcG9pbnQgdGhhdCBzZXJ2ZXMgSlNPTiBkYXRhIHdpdGggdGhlIGl0ZW1zIGFycmF5IHRvIGJlIGltcG9ydGVkXG4gKi9cbmNvbnN0IGxvYWRfZGF0YSA9IGZ1bmN0aW9uKGVuZHBvaW50KVxue1xuICAgIGxldCBpdGVtcyA9IFtdO1xuXG4gICAgaWYoZW5kcG9pbnQpe1xuICAgICAgICBmZXRjaChlbmRwb2ludClcbiAgICAgICAgICAgIC50aGVuKCByZXNwb25zZSA9PiByZXNwb25zZS5qc29uKCkgKVxuICAgICAgICAgICAgLnRoZW4oIGRhdGEgPT4ge1xuICAgICAgICAgICAgICAgIGl0ZW1zID0gY3JlYXRlX2l0ZW1zKGRhdGEpO1xuICAgICAgICAgICAgICAgIGl0ZW1zX2xvb2t1cCA9IGl0ZW1zO1xuICAgICAgICAgICAgICAgIGluaXRfbWFwKCBpdGVtcyApO1xuICAgICAgICAgICAgICAgIGluaXRfbGVnZW5kKCBpdGVtcyApO1xuICAgICAgICAgICAgICAgIHVwZGF0ZV90aW1lbGluZSggaXRlbXMgKTtcbiAgICAgICAgICAgICAgICBpZih3aW5kb3cubG9jYXRpb24uaGFzaCkge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBoYXNoID0gd2luZG93LmxvY2F0aW9uLmhhc2guc3Vic3RyaW5nKDEpO1xuICAgICAgICAgICAgICAgICAgICBzZWxlY3RfaXRlbShoYXNoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdmFyIGxhc3RfZWRpdGlvbj0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLmxlZ2VuZC1jYXRlZ29yeV9faXRlbXNbZGF0YS1jYXQ9XCJ5ZWFyXCJdIGxpOmxhc3QtY2hpbGQnKTtcbiAgICAgICAgICAgICAgICBzZWxlY3RfbGVnZW5kX2NhdGVnb3J5KCBsYXN0X2VkaXRpb24gKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnN0IGl0ZW1zX3NpemUgPSAyMDtcbiAgICAgICAgaXRlbXMgPSBuZXcgQXJyYXkoaXRlbXNfc2l6ZSkuZmlsbCgpLm1hcCggKGksIG4pID0+ICh7XG4gICAgICAgICAgICBwayAgICAgOiBuLFxuICAgICAgICAgICAgbmFtZSAgIDogbG9yZW0uZ2VuZXJhdGVXb3JkcyggcmludCgzLCA4KSApXG4gICAgICAgICAgICAgICAgICAgICAgICAgIC5yZXBsYWNlKCAvXlxcdy8sIGMgPT4gYy50b1VwcGVyQ2FzZSgpICksIC8vIGNhcGl0YWxpemUgYW55IHNlbnRlbmNlXG4gICAgICAgICAgICBib2R5ICAgOiBsb3JlbS5nZW5lcmF0ZVNlbnRlbmNlcyggcmludCgzLCA1KSApLFxuICAgICAgICAgICAgaW1nICAgIDogJy9zdGF0aWMvc2l0ZS9pbWcvYmxhbmstaW1hZ2Uuc3ZnJyxcbiAgICAgICAgICAgIGNhdHMgICA6IHJhbmRvbV9jYXRzKDIpLFxuICAgICAgICAgICAgZGF0ZSAgIDogcmFuZG9tX2RhdGUoXCIyMDIwLTAxLTAxXCIpLFxuICAgICAgICAgICAgbGF0bG9uIDogcmFuZG9tX2xhdGxvbiggTUFQX0RFRkFVTFRTLnZpZXdbMF0gKSxcbiAgICAgICAgICAgIGxuayAgICA6ICcvJyxcbiAgICAgICAgfSkpLnNvcnQoKGEsYikgPT4gYi5kYXRlIC0gYS5kYXRlKTtcbiAgICAgICAgaXRlbXMuZm9yRWFjaChpID0+IHtcbiAgICAgICAgICAgIGkuZGF0ZSA9IG5ldyBEYXRlKGkuZGF0ZSkudG9Mb2NhbGVEYXRlU3RyaW5nKCdlcy1FUycpO1xuICAgICAgICAgICAgaS50ZXJtcyA9IFtdO1xuICAgICAgICAgICAgZm9yKGNvbnN0IGNhdCBpbiBpLmNhdHMpe1xuICAgICAgICAgICAgICAgIGkuY2F0c1tjYXRdLmZvckVhY2godGVybSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGkudGVybXMucHVzaCh0ZXJtKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuICAgICAgICBpdGVtc19sb29rdXAgPSBpdGVtcztcbiAgICAgICAgaW5pdF9tYXAoIGl0ZW1zICk7XG4gICAgICAgIGluaXRfdGltZWxpbmUoIGl0ZW1zICk7XG4gICAgICAgIGluaXRfbGVnZW5kKCBpdGVtcyApO1xuICAgIH1cbn1cblxud2luZG93Lm9ubG9hZCA9ICgpPT57XG4gICAgbG9hZF9kYXRhKGAvYXBpLzEuMC90ZXN0aW1vbmllcz9sYW5nPSR7bGFuZ31gKTtcbiAgICBjb25zdCBub3RfcmVwbHlfdmlkZW9zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgndmlkZW86bm90KC52aWRlby1yZXBseSknKTtcbiAgICBub3RfcmVwbHlfdmlkZW9zLmZvckVhY2goIGZ1bmN0aW9uKHZpZGVvKXtcbiAgICAgICAgdmFyIHBrID0gdmlkZW8ucGFyZW50Tm9kZS5wYXJlbnROb2RlLmRhdGFzZXQucGs7XG4gICAgICAgIHZpZGVvLmFkZEV2ZW50TGlzdGVuZXIoJ3BsYXknLCBmdW5jdGlvbihlKXtcbiAgICAgICAgICAgIHNlbGVjdF9pdGVtKHBrKTtcbiAgICAgICAgICAgIHZpZGVvcy5mb3JFYWNoKGZ1bmN0aW9uKGkpe1xuICAgICAgICAgICAgICAgIGlmKGkgIT0gZS50YXJnZXQpIHtcbiAgICAgICAgICAgICAgICAgICAgaS5wYXVzZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9KTtcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcudGltZWxpbmUtaXRlbV9fYmFjaycpLmZvckVhY2goIGZ1bmN0aW9uKGkpe1xuICAgICAgICBpLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oZSl7XG4gICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgIHNob3dfYWxsX3RpbWVsaW5lX2l0ZW1zKCk7XG4gICAgICAgIH0pXG4gICAgfSk7XG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnNpdGUtbG9nbycpLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oZSl7XG4gICAgICAgIHNob3dfYWxsX3RpbWVsaW5lX2l0ZW1zKCk7XG4gICAgfSk7XG4gICAgY29uc3QgaG9vZHNlYXJjaCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zZWFyY2gtaG9vZF9fd2lkZ2V0Jyk7XG4gICAgaWYoaG9vZHNlYXJjaCkgaG9vZHNlYXJjaC5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCBmdW5jdGlvbihlKXtcbiAgICAgICAgdmFyIGxhdGxvbiA9IGhvb2RzZWFyY2gudmFsdWUuc3BsaXQoXCIsXCIpO1xuICAgICAgICBtYXAuc2V0VmlldyhsYXRsb24sIDE3KTtcbiAgICB9KTtcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcudGltZWxpbmUtaXRlbScpLmZvckVhY2goIGZ1bmN0aW9uKGkpe1xuICAgICAgICBpLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24oZSl7XG4gICAgICAgICAgIGlmKCFpLmNsYXNzTGlzdC5jb250YWlucygnc2VsZWN0ZWQnKSkge1xuICAgICAgICAgICAgICBzZWxlY3RfaXRlbShpLmRhdGFzZXQucGspO1xuICAgICAgICAgICB9XG4gICAgICAgIH0pXG4gICAgfSk7XG59O1xuIl0sInNvdXJjZVJvb3QiOiIifQ==