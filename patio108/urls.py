# django
from django.contrib import admin
from django.urls import path
from django.conf.urls.i18n import i18n_patterns 
from django.conf.urls import include
from django.views.generic import TemplateView
from django.conf.urls.static import static
# project
from django.conf import settings
from apps.connectors import urls as webhooks
from apps.api import urls as api
from apps.map.views import Map
from django.contrib.auth.views import LogoutView
from django.conf import settings

urlpatterns = [
    # admin
    path(
        'admin/',
        admin.site.urls
    ),
    path(
        'i18n/', 
        include('django.conf.urls.i18n')
    ),
    path(
        'logout',
        LogoutView.as_view(),
        { 'next_page': settings.LOGOUT_REDIRECT_URL },
        name='logout'
    ),
    # CKEditor
    path(
        'ckeditor/',
        include('ckeditor_uploader.urls')
    ),
    # webhooks
    path(
        'webhooks/',
        include(
            webhooks,
            namespace="webhooks"
        )
    ),
    # API
    # v1.0
    path(
        'api/1.0/',
        include(
            api,
        )
    ),
]

urlpatterns += i18n_patterns(
    # map related URLs
    path(
        '',
        Map.as_view(),
        name="map"
    ),
)

# Add static URLS when running a standalone server through manage.py
if settings.DEBUG == True:
    urlpatterns += static(
        settings.STATIC_URL,
        document_root = settings.STATIC_ROOT
    )
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root = settings.MEDIA_ROOT
    )
