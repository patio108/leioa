# python
import os.path
# django
from django import template
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.middleware.csrf import get_token
# project
from django.conf import settings

register = template.Library()

@register.simple_tag
def t(obj, field, lang=None):    
    if obj.__class__.__name__ == 'dict':
       return obj.get(field)
    trans = getattr(obj, '%s_%s' % ( field, lang), None)
    if lang != 'eu' and trans:
        return mark_safe( trans )
    return mark_safe( 
        getattr(obj, field)  
    )