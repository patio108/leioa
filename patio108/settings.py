"""
Django settings for patio108 project.
"""
import os
from django.utils import lorem_ipsum as lorem
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Assets (CSS, JavaScript, Images)
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'patio108', 'static'),
]

STATIC_ROOT = os.path.join(BASE_DIR, 'assets/static')
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'assets/media')
MEDIA_URL = '/media/'

PROJECT_ADMIN_APPS = []

LOGOUT_REDIRECT_URL = '/'

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

CONTRIB_APPS = [
    'leaflet',
    'djgeojson',
    'ckeditor',
    'ckeditor_uploader',
    'imagekit',
    'adminsortable',
    'rest_framework',
    'django.contrib.humanize'
]

PROJECT_APPS = [
    'apps.model_utils',
    'apps.map',
    'apps.connectors',
    'apps.api',
    'apps.textblock'
]

INSTALLED_APPS = PROJECT_ADMIN_APPS + DJANGO_APPS + CONTRIB_APPS + PROJECT_APPS

# Middleware

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'patio108.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            'patio108/templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'patio108.context_processors.site_info_processor',
                'patio108.context_processors.debug_processor',
            ],
            'builtins' : [
                'patio108.templatetags.patio108',
            ]
        },
    },
]

WSGI_APPLICATION = 'patio108.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'es'
TIME_ZONE = 'Europe/Madrid'
USE_I18N = True
USE_L10N = True
USE_TZ = True
LANGUAGES = [
  ('es', _('Español')),
  ('eu', _('Euskera')),
]
LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale'),
]

# CKEDITOR: Image uploader
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_JQUERY_URL  = "/static/admin/js/vendor/jquery/jquery.min.js"

#
# Document settings
#

DOCUMENT_TITLE = 'Ekin Leioa'
DOCUMENT_DESCRIPTION = _(
    'EKIN Leioa es un espacio de trabajo colaborativo entre la administración '
    'y la ciudadanía, donde desarrollar a partir de las necesidades comunes, '
    'ideas e iniciativas propuestas por las vecinas y vecinos que ayuden a '
    'mejorar el bienestar y la salud de todas.'
)


# Leaflet
LEAFLET_CONFIG = {
    'DEFAULT_CENTER': ( 43.3246031, -2.9769796 ),
    'DEFAULT_ZOOM'  : 13,
    'MIN_ZOOM'      : 2,
    'MAX_ZOOM'      : 18,
}

#
# Import private settings
#

from .private_settings import *
